#!/usr/bin/python3

import sys
sys.path.append('./')
import DIR
import numpy as np
import os
from astropy.io import fits
from scipy import stats

photo_sample = fits.open('./tests/data/photo_data.fits')[1].data
spec_sample = fits.open('./tests/data/spec_data.fits')[1].data


def test_remove_further_duplicates():
    photo_index = np.array([3, 2, 1, 3, 1, 5])
    dist_2d = np.array([0.2, 1, 1, 1.5, 0.1, 2.5])

    pi, d2d, sel = DIR.remove_further_duplicates(photo_index, dist_2d)

    assert(np.all(pi == [3, 2, 1, 5]))
    assert(np.all(d2d == [0.2, 1, 0.1, 2.5]))

    assert(np.all(pi == photo_index[sel]))
    assert(np.all(d2d == dist_2d[sel]))


def test_cross_match_photo_spec():
    columns = ['ra', 'dec']
    pcat_xmat, scat_xmat = DIR.cross_match_photo_spec(spec_sample, spec_sample,
                                                      columns, columns)
    assert(len(spec_sample) == len(pcat_xmat))
    assert(len(spec_sample) == len(scat_xmat))

    pcat_xmat, scat_xmat = DIR.cross_match_photo_spec(photo_sample,
                                                      spec_sample, columns,
                                                      columns)
    assert(len(pcat_xmat) == len(scat_xmat))
    # This is actually testing for no duplicated matches, as well. Note that if
    # the code were not removing double cross_matching, the cross-matched
    # elements would be 2 items larger
    assert(len(photo_sample) == 2 * len(pcat_xmat))
    assert(len(spec_sample) == 2 * len(scat_xmat))

    # Test return_ix_mat
    pcat_xmat, scat_xmat, pix_mat, spec_ix_xmat = DIR.cross_match_photo_spec(photo_sample,
                                                               spec_sample,
                                                               columns,
                                                               columns,
                                                               return_ix_xmat=True)
    assert(np.all(photo_sample[pix_mat] == pcat_xmat))
    assert(np.all(spec_sample[spec_ix_xmat] == scat_xmat))

    # Test that when removing multiple cross-matchings, the furthest ones are removed.
    assert(pcat_xmat['ra'][0] == photo_sample['ra'][0])
    assert(pcat_xmat['dec'][1] == photo_sample['dec'][1])
    assert(scat_xmat['ra'][0] == spec_sample['ra'][0])
    assert(scat_xmat['dec'][1] == spec_sample['dec'][1])


def test_distance_to_nearest_neighbors():
    size = int(len(photo_sample) / 2)
    d = DIR.distance_to_nearest_neighbors(photo_sample[:size], ['mag'], size)
    assert(np.isclose(d.min(), 1, rtol=1e-3))
    assert(np.isclose(d.max(), 2, rtol=1e-3))
    d = DIR.distance_to_nearest_neighbors(photo_sample[:size], ['mag'],
                                          int(size/2))
    assert(np.isclose(d.min(), 0.5, atol=0.01, rtol=1e-2))
    assert(np.isclose(d.max(), 1, atol=0.01, rtol=1e-2))


def test_elements_within_radius():
    columns = ['ra', 'dec']
    pcat_xmat, scat_xmat = DIR.cross_match_photo_spec(photo_sample,
                                                      spec_sample, columns,
                                                      columns)
    # The cross-matched elements are uniformly distributed in 'mag' from [-1,
    # 1], so the maximum separation will be 2.
    radius = np.ones(len(pcat_xmat)) * 2
    N = DIR.elements_within_radius(photo_sample, pcat_xmat, radius, ['mag'])
    assert(np.all(N == len(pcat_xmat)))

    # Test weights (which are 1/2 in the catalog)
    N = DIR.elements_within_radius(photo_sample, pcat_xmat, radius, ['mag'],
                                   weights_column='weight')
    assert(np.all(N == len(pcat_xmat) / 2))


def test_compute_weights():
    Ns_a = 20
    Np_a = np.random.uniform(0, 50, size=10000)
    weights = Np_a / Ns_a / len(photo_sample)
    weights2 = DIR.compute_weights(photo_sample, Np_a, Ns_a)
    assert(np.all(weights == weights2))
    assert(os.path.isfile('_weights.npz') is False)

    # Check it saves output
    weights2 = DIR.compute_weights(photo_sample, Np_a, Ns_a, output='test')
    assert(os.path.isfile('test_weights.npz'))
    assert(np.all(weights == weights2))

    # Check it ignores Ns_a and reads saved output
    Ns_a = 10
    weights2 = DIR.compute_weights(photo_sample, Np_a, Ns_a, output='test')
    assert(np.all(weights == weights2))
    os.remove('test_weights.npz')


def test_compute_Nz():
    size = int(len(spec_sample) / 2)
    weights = np.ones(2 * size)
    pNz = DIR.compute_Nz(photo_sample, weights)

    # Check that pNz is a density normalized to 1
    # We check the reldev due to possible numerical errors
    assert(np.abs(np.sum(pNz[0] * np.diff(pNz[1])) / 1 - 1) < 1e-5)

    # Check that we can recover a normal distribution from the histogram
    pNz_dist = stats.rv_histogram(pNz)
    pNz_dist_sample = pNz_dist.rvs(size=10000)
    pNz_dist_sample[pNz_dist_sample < 0] = 0
    _, pvalue = stats.normaltest(pNz_dist_sample)
    assert(pvalue > 0.05)


def test_do_DIR():
    columns_sky = ['ra', 'dec']
    columns_mag = ['mag']

    pcat_xmat, scat_xmat = DIR.cross_match_photo_spec(photo_sample,
                                                      spec_sample, columns_sky,
                                                      columns_sky)

    Nz = DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat, columns_mag, 'z')


    size = int(len(spec_sample) / 2)
    Nz2 = DIR.compute_Nz(spec_sample[:size], np.ones(size))

    assert(np.max(np.abs((np.array(Nz[0]) + 1e-100) / (np.array(Nz2[0]) +
                                                       1e-100) - 1)) < 1e-2)

    # Check galaxy weighting works

    Nz2 = 2 * DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat, columns_mag, 'z',
                         normalized_to_xmat=True, weights_column='weight')
    assert(np.max(np.abs((np.array(Nz[0] * len(pcat_xmat)) + 1e-100) /
                         (np.array(Nz2[0]) + 1e-100) - 1)) < 1e-2)

    # Check it saves output
    Nz2 = DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat, columns_mag, 'z',
                    output='test')
    assert(os.path.isfile('test_Nz.npz'))
    assert(np.max(np.abs((np.array(Nz[0]) + 1e-100) / (np.array(Nz2[0]) +
                                                       1e-100) - 1)) < 1e-2)

    # Check it ignores spec_z_column and reads saved output
    Nz2 = DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat, columns_mag, 'pepito',
                     output='test')
    assert(np.max(np.abs((np.array(Nz[0]) + 1e-100) / (np.array(Nz2[0]) +
                                                       1e-100) - 1)) < 1e-2)

    os.remove('test_Nz.npz')
    os.remove('test_weights.npz')

    # Check if N(z) is normalized_to_xmat
    Nz2 = DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat, columns_mag, 'z',
                    output='test', normalized_to_xmat=True)
    assert(np.max(np.abs((np.array(Nz[0] * len(pcat_xmat)) + 1e-100) /
                         (np.array(Nz2[0]) + 1e-100) - 1)) < 1e-2)

    # Check it works also when read from saved file
    Nz2 = DIR.do_DIR(None, None, None, columns_mag, 'pepito',
                     output='test', normalized_to_xmat=True)
    assert(np.max(np.abs((np.array(Nz[0] * len(pcat_xmat)) + 1e-100) /
                         (np.array(Nz2[0]) + 1e-100) - 1)) < 1e-2)

    os.remove('test_Nz.npz')
    os.remove('test_weights.npz')
