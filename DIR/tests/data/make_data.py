#!/usr/bin/python

import numpy as np
from astropy.io import fits


def make_photo_data():
    size = int(1e4)
    dec = np.random.uniform(-90, 90, size)
    ra = np.random.uniform(-180, 180, size)
    z = 0.05 * np.random.randn(size) + 1
    z[z < 0] = 0
    weight = np.ones_like(z) / 2
    mag = np.concatenate((np.random.uniform(-1, 1, int(size/2)),
                         np.random.uniform(10, 15, int(size/2))))

    cols = fits.ColDefs([fits.Column(name='ra', format='D', array=ra),
                         fits.Column(name='dec', format='D', array=dec),
                         fits.Column(name='z', format='D', array=z),
                         fits.Column(name='mag', format='D', array=mag),
                         fits.Column(name='weight', format='D', array=weight),
                         ])

    hdu = fits.BinTableHDU.from_columns(cols)
    hdu.writeto("./photo_data.fits", overwrite=True)

    return hdu


def make_spec_data(pdata):
    ra = pdata['ra']
    dec = pdata['dec']
    z = pdata['z']
    z[int(ra.size/2):] = 0.1 * np.random.randn(int(ra.size/2)) + 1.5
    z[z < 0] = 0

    size = (2, int(ra.size/2))
    displacement = np.zeros((2, ra.size))
    # We will move half of the galaxies < 1arsec. Use 0.1 to make sure <1arcsec
    displacement[:, :size[1]] = np.random.uniform(-0.1, 0.1, size=size) / 3600
    # We will displace the other galaxies > 1arcsec.
    sign = np.random.uniform(size=size)
    sign[sign > 0.5] = 1
    sign[sign < 0.5] = -1
    displacement[:, size[1]:] = sign * np.random.uniform(1, 10, size=size) / 3600
    ra += displacement[0]
    dec += displacement[1]
    # From these that are displaced > 1arcsec and will not be cross-matched, we
    # modified a few of them so that there is multiple cross-matching, but
    # slightly more than in the first half, so that we don't have to modify the
    # test that might compare them
    ra[-1] = ra[0] + 1.1 * displacement[0, 0]
    ra[-2] = ra[1] + 1.1 * displacement[0, 1]
    dec[-1] = dec[0] + 1.1 * displacement[1, 0]
    dec[-2] = dec[1] + 1.1 * displacement[1, 1]

    dec[dec > 90] = 90
    dec[dec < -90] = -90
    ra[ra > 180] = 180
    ra[ra < -180] = -180

    cols = fits.ColDefs([fits.Column(name='ra', format='D', array=ra),
                         fits.Column(name='dec', format='D', array=dec),
                         fits.Column(name='ALPHA_J2000', format='D', array=ra),
                         fits.Column(name='DELTA_J2000', format='D', array=dec),
                         fits.Column(name='z', format='D', array=z),
                         ])

    hdu = fits.BinTableHDU.from_columns(cols)
    hdu.writeto("./spec_data.fits", overwrite=True)

    return hdu


def main():
    pdata = make_photo_data()
    sdata = make_spec_data(pdata.data)


if __name__ == '__main__':
    main()
