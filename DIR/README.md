# DIR

Training sample: [COSMOS2015](https://irsa.ipac.caltech.edu/data/COSMOS/overview.html) 30-band (Laigle et al. 2016, ApJS, 224, 24)

Samples:
 - METACALIBRATION: Overlaps
 - RedMaGiC: No overlap
 - KiDS-1000: No overlap with weak lensing catalog. Overlap with [this
     tile](http://ds.astro.rug.astro-wise.org:8000/KiDS_DR4.0_150.1_2.2_ugriZYJHKs_cat.fits). We will correct for differencies in the color space between this tile and the weak lensing catalog by double weighting: weights from tile x COSMOS times weights from tile2 x weak lensing catalog (suppossing the magnitude space of tile1 and 2 are similar)
 - DELS: Overlaps but we need to use the full Legacy Survey catalog. Folow https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations for cuts.

The area of overlap is the area of the COSMOS30 sample; i.e. 2deg^2.

