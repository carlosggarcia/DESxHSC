#!/usr/bin/python3

# from astropy.io import fits
import fitsio
import numpy as np
import DIR
import os
import healpy as hp

import time


def load_2MPZ(zmax=0.1):
    mask = hp.read_map('/mnt/extraspace/damonge/Datasets/2MPZ_WIxSC/WISExSCOSmask_galactic.fits.gz')

    path_photo_sample = '/mnt/extraspace/damonge/Datasets/2MPZ_WIxSC/2MPZ.fits'

    photo_sample = fitsio.read(path_photo_sample)
    photo_sample = photo_sample[photo_sample['ZPHOTO'] > 0]
    photo_sample = photo_sample[photo_sample['ZPHOTO'] <= zmax]

    nside = hp.npix2nside(mask.size)
    pix = hp.ang2pix(nside, photo_sample['L'], photo_sample['B'], lonlat=True)
    photo_sample = photo_sample[mask[pix] > 0]

    return photo_sample


def main():
    photo_sample = load_2MPZ()

    pcat_xmat = scat_xmat = spec_sample = photo_sample[photo_sample['ZSPEC'] > -1]

    # print(spec_sample[:10])
    # ldajdklfj

    photo_columns_mag = ['JCORR', 'KCORR', 'HCORR', 'W1MCORR', 'W2MCORR',
                         'BCALCORR', 'RCALCORR', 'ICALCORR']

    Nz = DIR.do_DIR(photo_sample, pcat_xmat, scat_xmat,
                    photo_columns_mag, 'ZSPEC',
                    output=f'output/2MPZ/2MPZ_new2', zmax=0.4)


def main2():
    import sys
    sys.path.append('../../xCell')
    import xcell as xcl
    import yaml
    import healpy as hp
    import matplotlib.pyplot as plt
    import time
    config = {'data_catalog': '/mnt/extraspace/damonge/S8z_data/WIxSC/2MPZ.fits',
              'mask': '/mnt/extraspace/damonge/S8z_data/WIxSC/WISExSCOSmask.fits.gz',
              'z_edges': [0.0, 0.1],
              'path_rerun': './',
              'n_jk_dir': 100,
              'mask_name': 'mask_LOWZ',
              'mapper_class': 'Mapper2MPZ',
              'nside': 512,
              'bias': 1.182}
    mp = xcl.mappers.Mapper2MPZ(config)
    d = mp.get_signal_map()
    m = mp.get_mask()
    start = time.time()
    x = mp.get_nz()
    stop = time.time()
    print(stop-start)


if __name__ == "__main__":
    main()
