#!/bin/bash

queue=cmb
n=12

# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS adjoint" -o log/DES_full_sample_adjoint.out /usr/bin/python3 DES_Nz.py --full_sample --adjoint_colors output/DES/DES
# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS riz" -o log/DES_full_sample_riz.out /usr/bin/python3 DES_Nz.py --full_sample --colors riz output/DES/DES
# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS R" -o log/DES_full_sample_metacal_R.out /usr/bin/python3 DES_Nz.py --full_sample --metacal_R output/DES/DES
# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz PAU FS R" -o log/DES_PAU_full_sample_metacal_R.out /usr/bin/python3 DES_Nz.py --PAU --full_sample --metacal_R output/DES/DES
# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz PAU FS" -o log/DES_PAU_full_sample.out /usr/bin/python3 DES_Nz.py --PAU --full_sample output/DES/DES
# addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS covNG" -o log/DES_full_sample_covng.out /usr/bin/python3 DES_Nz.py --full_sample --covng output/DES/DES

#addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS covNG COSMOS new cuts" -o log/DES_full_sample_covng_new_cuts.out /usr/bin/python3 DES_Nz.py --full_sample --covng --COSMOS15_less_cuts output/DES/DES
#addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS covNG COSMOS new cuts R" -o log/DES_full_sample_covng_new_cuts_metacalR.out /usr/bin/python3 DES_Nz.py --full_sample --covng --COSMOS15_less_cuts --metacal_R output/DES/DES
#addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz FS covNG R" -o log/DES_full_sample_covng_metacalR.out /usr/bin/python3 DES_Nz.py --full_sample --covng --metacal_R output/DES/DES

#addqueue -n 1x$n -s -m 10 -q $queue -c "DES Nz COSMOS15 cosmo_patch covNG R" -o log/DES_cosmo_patch_covng_metacalR.out /usr/bin/python3 DES_Nz.py --cosmo_patch --covng --metacal_R output/DES/DES

############
# Final run
######
# Chosen for cosmology: COSMOS15 with HSC cuts, cosmology patch, NG covariance and metacal weights
##
# addqueue -n 1x$n -s -m 15 -q $queue -c "DES Nz final" -o log/DES_final.out /usr/bin/python3 DES_Nz.py --cosmo_patch --covng --metacal_R output/DES_final/DES_final
######
# addqueue -n 1x$n -s -m 15 -q $queue -c "DES Nz final lc" -o log/DES_final.out /usr/bin/python3 DES_Nz.py --COSMOS15_less_cuts --cosmo_patch --covng --metacal_R output/DES_final/DES_final
######
# final with riz (ignore green band)
#addqueue -n 1x$n -s -m 15 -q $queue -c "DES Nz final lc" -o log/DES_final.out /usr/bin/python3 DES_Nz.py --cosmo_patch --covng --metacal_R --colors riz output/DES_final_riz/DES_final_riz
#addqueue -n 1x$n -s -m 15 -q $queue -c "DES Nz final xcell" -o log/DES_final_xcell.out /usr/bin/python3 DES_Nz.py --cosmo_patch --covng --metacal_R --colors riz --xCell output/DES_final_xCell/DES_final_xCell
