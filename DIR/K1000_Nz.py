#!/usr/bin/python3
import matplotlib
matplotlib.use('Agg')

import DIR
import numpy as np
import pymaster as nmt
import fitsio
import common as co
import os
import sacc
from matplotlib import pyplot as plt

photo_columns_sky = ['ALPHA_J2000', 'DELTA_J2000']
# No Z-band in tile
photo_columns_mag = [f'MAG_GAAP_{i}' for i in 'ugri']
photo_columns_mag_all = photo_columns_mag + [f'MAG_GAAP_{i}' for i in 'ZYJH'] + ['MAG_GAAP_Ks']

zbin_edges = np.array([[0.1, 0.3], [0.3, 0.5], [0.5, 0.7], [0.7, 0.9], [0.9,
                                                                        1.2]])






def load_catalogs():
    k1000_cos30_tile = fitsio.read('../quick_cell/KiDS_DR4.0_150.1_2.2_ugriZYJHKs_cat.fits',
                                   columns=photo_columns_sky + photo_columns_mag)

    k1000_cat = fitsio.read('/mnt/extraspace/damonge/S8z_data/KiDS_1000/KiDS_DR4.1_ugriZYJHKs_SOM_gold_WL_cat.fits',
                            columns=photo_columns_mag_all + ['Z_B', 'weight'])
    # for c in photo_columns_mag_all:
    #     k1000_cat = k1000_cat[k1000_cat[c] < 50]
    #     k1000_cat = k1000_cat[k1000_cat[c] >= 0]

    #     if c in photo_columns_mag:
    #         k1000_cos30_tile = k1000_cos30_tile[k1000_cos30_tile[c] < 50]
    #         k1000_cos30_tile = k1000_cos30_tile[k1000_cos30_tile[c] >= 0]

    for c in photo_columns_mag:
         k1000_cos30_tile = k1000_cos30_tile[k1000_cos30_tile[c] >= 0]

    # # Remove r > k1000_cat['MAG_GAAP_r'].max()
    # # rmax = k1000_cat['MAG_GAAP_r'].max()
    # rmax = 25 # k1000_cat['MAG_GAAP_r'].max()
    # k1000_cos30_tile = k1000_cos30_tile[k1000_cos30_tile['MAG_GAAP_r'] < rmax]
    # k1000_tile2 = k1000_tile2[k1000_tile2['MAG_GAAP_r'] < rmax]

    return k1000_cos30_tile, k1000_cat


def generate_sacc(Nz):
    print('Loading cls data sacc file', flush=True)
    path = '/mnt/zfsusers/gravityls_3/codes/montepython_cgg_emilio/cls_K1000_4096_zeroed_covG_v2.fits'
    s = sacc.Sacc.load_fits(path)

    for trs in s.get_tracer_combinations():
        if ('KiDS1000' not in trs[0]) or ('KiDS1000' not in trs[1]):
            s.remove_selection(tracers=trs)

    snew = sacc.Sacc()
    for i in range(len(zbin_edges)):
        trname = f'KiDS1000__{i}'
        Nzi, z_edges = Nz[i]
        z = (z_edges[:-1] + z_edges[1:]) / 2
        snew.add_tracer('NZ', trname, quantity=s.tracers[trname].quantity,
                        z=z, nz=Nzi)


    for dtype in s.get_data_types():
        for trs in s.get_tracer_combinations(data_type=dtype):
            ell, cl, ind = s.get_ell_cl(dtype, trs[0], trs[1], return_ind=True)
            bpw = s.get_bandpower_windows(ind)
            snew.add_ell_cl(dtype, trs[0], trs[1], ell, cl, window=bpw)

    snew.add_covariance(s.covariance.covmat)

    print('Saving new file with only DESwl and DIR N(z)', flush=True)
    snew.save_fits('output/cls_K1000_covG_DIR.fits', overwrite=True)


def main():
    Nz = []
    if not os.path.isfile('output/K1000_bin0_Nz.npz'):
        k1000_cos30_tile, k1000_cat = load_catalogs()
        photo_sample = k1000_cat
        spec_sample = co.load_cosmos()

        print('Cross matching', flush=True)
        pcat_xmat, scat_xmat, pix_xmat = DIR.cross_match_photo_spec(k1000_cos30_tile,
                                                            spec_sample['cat'],
                                                            photo_columns_sky,
                                                            spec_sample['columns_sky'],
                                                            return_pix_mat=True)
        print('Finished cross matching')

        for zbin in range(len(zbin_edges)):
            print(f'Selecting galaxies in zbin {zbin}', flush=True)
            # Based on https://github.com/xC-ell/xCell/blob/b00b448d18880afc3b405f939a7aabd934df45be/xcell/mappers/mapper_DESY1wl.py#L92-L96
            sel = ((photo_sample['Z_B'] > zbin_edges[zbin][0]) &
                   (photo_sample['Z_B'] <= zbin_edges[zbin][1]))

            print('doing DIR', flush=True)
            # zmax = 4 and nbins = 100 as in DES
            # Note, however, that there are some COSMOS outliers at z=10 that will
            # be ignored by setting zmax=4
            Nzi = DIR.do_DIR(photo_sample[sel], pcat_xmat, scat_xmat,
                            photo_columns_mag, spec_sample['z_column'], zmax=4, nbins=100,
                            output=f'output/K1000_bin{zbin}',
                             normalized_to_xmat=True, weights_column='weight')
            Nz.append(Nzi)
    else:
        for zbin in range(len(zbin_edges)):
            Nzi = DIR.do_DIR(None, None, None,
                            photo_columns_mag, 'PHOTOZ', zmax=4, nbins=100,
                            output=f'output/K1000_bin{zbin}',
                             normalized_to_xmat=True, weights_column='weight')
            Nz.append(Nzi)

    generate_sacc(Nz)

if __name__ == "__main__":
    main()
