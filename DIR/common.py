#!/usr/bin/python3

from numpy.lib.recfunctions import merge_arrays
import fitsio
import numpy as np
import DIR
import os
import sacc


def load_cosmos(only_column_names=False, asHSC=True):
    print('Loading COSMOS15')
    spec_columns_sky = ['ALPHA_J2000', 'DELTA_J2000']
    spec_z_column = 'PHOTOZ'

    cat = {'columns_sky': spec_columns_sky, 'z_column': spec_z_column}
    if only_column_names:
        return cat

    path_spec_sample = '/mnt/extraspace/damonge/Datasets/COSMOS/COSMOS2015_Laigle+_v1.1.fits'
    cat30 = fitsio.read(path_spec_sample) # , columns=spec_columns_sky + [spec_z_column])

    # Remove outliers
    if asHSC:
        # Copied from: https://github.com/LSSTDESC/DEHSC_LSS/blob/fc9c377bb397b58f693c2af75253e5c55fffb633/hsc_lss/cosmos_weight.py#L91-L95
        lim_indices=np.where((0.01 < cat30['PHOTOZ']) & (9 > cat30['PHOTOZ']) &
                             (cat30['TYPE'] == 0) & (cat30['ZP_2'] < 0) &
                             (cat30['MASS_BEST'] >7.5) &
                             (np.maximum(cat30['ZPDF_H68'] - cat30['ZPDF'],
                                         cat30['ZPDF'] - cat30['ZPDF_L68']) <
                              0.05 * (1 + cat30['PHOTOZ'])) &
                             (cat30['CHI2_BEST'] < cat30['CHIS']) &
                             (cat30['CHI2_BEST'] / (cat30['NBFILT'] + 1e-100) < 5.))
    else:
        lim_indices=np.where((0.01 < cat30['PHOTOZ']) & (9 > cat30['PHOTOZ']) &
                             (cat30['TYPE'] == 0))
    cat['cat'] = cat30[lim_indices]
    return cat


def load_PAU(only_column_names=False):
    print('Loading PAU')
    spec_columns_sky = ['ra', 'dec']
    spec_z_column = 'photoz'
    path_spec_sample = '/mnt/extraspace/damonge/Datasets/COSMOS/COSMOS_PAUS.fits'

    cat = {'columns_sky': spec_columns_sky, 'z_column': spec_z_column}

    if only_column_names:
        return cat

    cat['cat'] = fitsio.read(path_spec_sample)

    return cat


def load_spec_cat(catalog, only_column_names=False):
    if catalog == 'COSMOS15':
        cat = load_cosmos(only_column_names)
    elif catalog == 'COSMOS15_less_cuts':
        cat = load_cosmos(only_column_names, asHSC=False)
    elif catalog == 'PAU':
        cat = load_PAU(only_column_names)
    else:
        raise ValueError(f"Catalog {catalog} not implemented")

    return cat
