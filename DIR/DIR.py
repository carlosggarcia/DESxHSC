#!/usr/bin/python3

import numpy as np
import os
from astropy.coordinates import SkyCoord
from astropy.io import fits
from sklearn.neighbors import NearestNeighbors, BallTree
from numpy.lib import recfunctions as rfn


def columns_from_fits(catalog, columns):
    """
    Load columns from a catalog into an array

    Arguments
    ---------
        catalog (fits): Input fit file
        columns (list): List of columns names to return

    Returns
    -------
        array: Array with the selected columns extracted from the given catalog
    """
    return np.array([np.array(catalog[i]) for i in columns])


def remove_further_duplicates(photo_index, dist_2d):
    """
    Remove duplicates matches. Keep only the closest galaxy.

    Arguments
    ---------
        photo_index (array): Array of indices of photometric galaxies with
        spectroscopic counterpart; i.e. pcat_xmat = photo_sample[photo_index]
        dist_2d  (Angle): The on-sky separation between the closest match for
        each element in this object in catalogcoord. Shape matches this object.

    Returns
    -------
        photo_index (array): Subset of the input photo_index array without
        duplicates.
        dist_2d (Angle): The corresponding dist_2d object.
        sel (array): The selection array to remove the duplicates; e.g.
        the returned photo_index = photo_index[sel]

    """
    pi_unique, count = np.unique(photo_index, return_counts=True)
    indices_repeated = pi_unique[count > 1]

    sel = np.array([True] * photo_index.size)
    for i in indices_repeated:
        ix_in_photo_index = np.where(photo_index == i)
        # Keep closest galaxy
        ix_to_keep = dist_2d[ix_in_photo_index].argmin()
        ix_to_delete = np.delete(ix_in_photo_index, ix_to_keep)
        sel[ix_to_delete] = False

    return photo_index[sel], dist_2d[sel], sel


def cross_match_photo_spec(photo_sample, spec_sample, photo_columns,
                           spec_columns, return_ix_xmat=False, max_distance=1):
    """
    Match the galaxies in both photo_sample and spec_sample.

    Arguments
    ---------
        photo_sample (fits): Photometric sample.
        spec_sample (fits): Spectroscopic sample.
        photo_columns (list): List with the column names of the right ascension
        and declination columns for the photo_sample. They are asumed to be in
        degrees.
        spec_columns (list): Same as photo_columns but for the spec_sample.
        return_ix_xmat (bool): If True return the indices that slice the
        photo_sample and spec_sample after cross-matching. Default False.
        max_distance (float): Maximum sky separation for matching in arcsec.
        Default 1 arcsec
    Returns
    -------
        fits: photo_xmat: Subsample of the photometric sample that
        cross-matches with the spectroscopic
        fits: spec_xmat: As above, but for the spectroscopic sample
        array: pix_xmat: Array with the indices of the galaxies in the
        photometric sample with spectroscopic counterpart.
    """
    pra, pdec = columns_from_fits(photo_sample, photo_columns)
    sra, sdec = columns_from_fits(spec_sample, spec_columns)
    # Based on
    # https://github.com/LSSTDESC/DEHSC_LSS/blob/master/hsc_lss/cosmos_weight.py
    # Match coordinates
    photo_skycoord = SkyCoord(ra=pra, dec=pdec, unit='deg')
    spec_skycoord = SkyCoord(ra=sra, dec=sdec, unit='deg')


    # Nearest neighbors
    # Cross-match from spec to photo, not photo to spec
    photo_index, dist_2d, _ = \
        spec_skycoord.match_to_catalog_sky(photo_skycoord)

    # Cut everything further than 1 arcsec
    mask = dist_2d.degree * 60 * 60 < max_distance
    pix_xmat = photo_index[mask]
    photo_xmat = photo_sample[pix_xmat]
    spec_xmat = spec_sample[mask]

    # Check if there are multiple cross-matchings
    rdev = pix_xmat.size / np.unique(pix_xmat).size - 1
    print(f'Multiple cross-matching: {100 * rdev:.2f}%', flush=True)

    if np.abs(rdev) > 0:
        print('Removing multiple cross-matching', flush=True)
        pix_xmat, dist_2d_xmat, sel = remove_further_duplicates(pix_xmat,
                                                                dist_2d[mask])
        # Update mask
        ix_to_remove = np.where(~sel)[0]
        ix_true_in_mask = np.where(mask)[0]
        mask[ix_true_in_mask[ix_to_remove]] = False

        rdev = pix_xmat.size / np.unique(pix_xmat).size - 1
        print(f'Multiple cross-matching after cleaning: {100 * rdev:.2f}%',
              flush=True)

        photo_xmat, spec_xmat = photo_xmat[sel], spec_xmat[sel]

    if return_ix_xmat:
        return photo_xmat, spec_xmat, pix_xmat, mask
    else:
        return photo_xmat, spec_xmat


def distance_to_nearest_neighbors(sample, columns, nneigh=20):
    """
    Find the distance to the nearest neighbors. The distance is given by Eq. 23
    of https://arxiv.org/pdf/0801.3822.pdf.

    Arguments
    ---------
        sample (fits): Input sample
        columns (list): List of columns names to compute the distance
        nneigh (int): Number of nearest neighbors to compute the distance for.
        Default is 20.
    Returns
    -------
        array: Array with the distances to the nearest neighbors for each
        element in sample.
    """
    train_sample = columns_from_fits(sample, columns).T
    n_nbrs = NearestNeighbors(n_neighbors=nneigh, algorithm='kd_tree',
                              metric='euclidean', n_jobs=-1).fit(train_sample)

    distances, _ = n_nbrs.kneighbors(train_sample)
    # Get maximum distance
    distances = np.amax(distances, axis=1)

    return distances


def elements_within_radius(photo_sample, xmatch_sample, radius, columns, weights_column=None):
    """
    Find the number of elements that lie within the given radius

    Arguments
    ---------
        photo_sample (fits): Photometric sample
        xmatch_sample (list): Cross-matched sample. It does not need to be a subsample of photo_sample.
        radius (array): Array of distances to count the number of elments within.
        columns (list): List of columns names to use to compute the distances.
        weights_column (str): Galaxies weights column name.

    Returns
    -------
        array: Array with the number of nearest elements within the given
        radius.
    """

    photo_sample_cols = columns_from_fits(photo_sample, columns).T
    xmatch_sample_cols = columns_from_fits(xmatch_sample, columns).T

    # Using BallTree and query_radius to avoid the need of having the data
    # points in the photo_sample
    tree_NN_lookup = BallTree(photo_sample_cols, leaf_size=40)
    ix_NN = tree_NN_lookup.query_radius(xmatch_sample_cols, radius+1E-6)
    if weights_column:
        num_photoz = np.array([np.sum(photo_sample[ix][weights_column]) for ix in
                                      ix_NN])
    else:
        num_photoz = np.array([len(ix) for ix in ix_NN])

    return num_photoz


def compute_weights(photo_sample, Np_a, Ns_a, output='', weights_column=None):
    """
    Compute the weights given by Eq. 24 of https://arxiv.org/pdf/0801.3822.pdf.

    Arguments
    ---------
        photo_sample (fits): Photometric sample
        Np_a (array): Number of nearest neighbors around a given element of the
        photometric sample.
        Ns_a (array): Number of nearest neighbors used to compute the distance in
        the spectroscopic sample and the numbers of elements within it in the
        photometric sample.
        output (str): Path + name that will have the weights file. It will be
        called output + 'weights.npz'

    Returns
    -------
        array: Array of DIR weights
    """
    fname = output + '_weights.npz'
    if os.path.isfile(fname):
        W_a = np.load(fname)['W_a']
    else:
        if weights_column:
            Np_tot = np.sum(photo_sample[weights_column])
        else:
            Np_tot = len(photo_sample)
        W_a = Np_a / Ns_a / Np_tot

        if output:
            np.savez(fname, W_a=W_a)

    return W_a


def compute_Nz(spec_xmatch, weights, z_column='z', zmax=None, nbins=100):
    """
    Compute the redshift distribution N(z) given by Eq. 12 of
    https://arxiv.org/pdf/0801.3822.pdf.

    Arguments
    ---------
        spec_xmatch (fits): Spectroscopic sample after crossmatch with the
        Spectroscopic sample. If Nz should be computed for a given redshift
        bin. photo_xmatch should be the subsample corresponding to that
        redshift bin.
        weights (array): DIR weights
        z_column (str): Column name in the spec_xmatch fits file
        corresponding to the galaxies redshift.
        nbins (int): Number of redshift bins for the histogram

    Returns
    -------
        array: histogram with normalized N(z)
        array: z-bin edges
    """
    z_a = spec_xmatch[z_column]

    if zmax is None:
        zmax = z_a.max()
    return np.histogram(z_a, bins=nbins, range=(0, zmax),
                        weights=weights, density=True)


def do_DIR(photo_sample, pcat_xmat, scat_xmat,
           photo_columns_mag, spec_z_column, nneigh=20, nbins=100, zmax=None,
           output='', normalized_to_xmat=False, weights_column=None,
           adjoint_colors=False, color_col=None):
    """
    Compute the redshift distribution, N(z), of a photometric sample using a
    spectroscopic sample of reference with DIR.

    Arguments
    ---------
        photo_sample (fits): Full photometric sample.
        pcat_xmat (fits): Cross-matched photometric sample
        scat_xmat (fits): Cross-matched spectroscopic sample.
        xmatch_function (function): Function that accepts photo_sample and
        spec_sample as arguments and returns the photometric and spectroscopic
        cross matched samples
        photo_columns_mag (list): List of the column names for the magnitude/fluxes
        columns to use in the photometric sample.
        spec_z_column (str): Column name for spectroscopic redshifts.
        nneigh (int): Number of neighbors to compute the distance to.
        zmax (float): Maximum z to histogram
        output (str): Path + name that will have the weights and Nz file. They
        will be called output + 'weights.npz' and output + 'Nz.npz'
        normalized_to_xmat (bool): If True return the histogram normalized so
        that \sum_i Nz_i dz_i = N_gals in xmat sample. Default False (\sum_i
        Nz_i dz_i= 1)
        weights_column (str): Galaxies weights column name.
    Returns
    -------
        array: histogram with normalized N(z)
        array: z-bin edges
    """
    fname = output + '_Nz.npz'
    if os.path.isfile(fname):
        print('Reading Nz', flush=True)
        Nz_file = np.load(fname)
        Nz = (Nz_file['Nz'], Nz_file['z'])
        norm = Nz_file['norm']
    else:
        if adjoint_colors:
            new_columns = []
            new_data = []
            new_data_ixmat = []
            for ci, cj in zip(photo_columns_mag[:-1], photo_columns_mag[1:]):
                ac = f'{ci}-{cj}'
                new_columns.append(ac)
                # This doesn't work for structured arrays
                # photo_sample[ac] = photo_sample[ci] - photo_sample[cj]
                new_data.append(photo_sample[ci] - photo_sample[cj])
                new_data_ixmat.append(pcat_xmat[ci] - pcat_xmat[cj])
            # This does
            photo_sample = rfn.append_fields(photo_sample, new_columns, new_data, usemask=False)
            pcat_xmat = rfn.append_fields(pcat_xmat, new_columns, new_data_ixmat, usemask=False)
            photo_columns_mag = new_columns
            if color_col is not None:
                photo_columns_mag.append(color_col)

        print('Starting distance_to_nearest_neighbors', flush=True)
        distance = distance_to_nearest_neighbors(pcat_xmat, photo_columns_mag,
                                                 nneigh=nneigh)

        print('Starting elements_within_radius', flush=True)
        Np_a = elements_within_radius(photo_sample, pcat_xmat, distance,
                                      photo_columns_mag, weights_column)

        print('Starting compute_weights', flush=True)
        weights = compute_weights(photo_sample, Np_a, nneigh, output, weights_column)

        # if weights_column:
        #     weights *= pcat_xmat[weights_column]

        print('Starting compute_Nz', flush=True)
        Nz = compute_Nz(scat_xmat, weights, spec_z_column, zmax, nbins)

        norm = len(pcat_xmat)

        if output:
            print('Saving Nz', flush=True)
            np.savez(fname, Nz=Nz[0], z=Nz[1], norm=norm)

    if normalized_to_xmat:
        Nz = (Nz[0] * norm, Nz[1])

    return Nz
