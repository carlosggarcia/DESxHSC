#!/usr/bin/python3

# from astropy.io import fits
# from astropy.table import Table, hstack
from numpy.lib import recfunctions as rfn
import fitsio
import numpy as np
import DIR
import os
import sacc
import common as co


def load_DES(ra=None, dec=None, suffix=None, only_column_names=False, riz=False, xCell=False):
    print('Loading DES')
    photo_columns_sky = ['ra', 'dec']
    photo_columns_mag = ['mag_g', 'mag_r', 'mag_i', 'mag_z'][riz:]
    photo_columns_flux = ['flux_g', 'flux_r', 'flux_i', 'flux_z'][riz:]
    photo_columns_zbin = ['zbin_mcal']

    cat = {'columns_sky': photo_columns_sky, 'z_column': photo_columns_zbin[0],
           'columns_mag': photo_columns_mag}

    if only_column_names:
        return cat

    if xCell:
        suffix += '_xCell'

    if riz:
        suffix += '_riz'

    path_photo_short = f'/mnt/extraspace/gravityls_3/data/DESY1/DESY1wl_short_for_DIR_with_{suffix}.fits'
    if os.path.isfile(path_photo_short):
        print('Reading short photometric catalogs')
        cat_photo = fitsio.read(path_photo_short)
    else:
        path_photo_sample = '/mnt/extraspace/gravityls_3/data/DESY1/y1_metacal_flux_griz_release.fits'
        path_photo_sample_sky = '/mnt/extraspace/damonge/Datasets/DES_Y1/shear_catalog/mcal-y1a1-combined-riz-unblind-v4-matched.fits'
        path_photo_sample_zbin = '/mnt/extraspace/damonge/Datasets/DES_Y1/shear_catalog/y1_source_redshift_binning_v1.fits'

        # Not the most efficient but the easiest option
        if xCell:
            path_photo_sample = path_photo_sample_sky

        print('Reading 1', flush=True)
        photo_sample = fitsio.read(path_photo_sample, columns=photo_columns_flux)
        print('Reading 2', flush=True)
        photo_sample_sky = fitsio.read(path_photo_sample_sky, columns=photo_columns_sky + ['flags_select', 'R11', 'R12', 'R21', 'R22'])
        print('Reading 3', flush=True)
        photo_sample_zbin = fitsio.read(path_photo_sample_zbin, columns=photo_columns_zbin)

        print('Finished reading', flush=True)

        # Remove flagged galaxies
        sel = photo_sample_sky['flags_select'] == 0
        # Remove galaxies that don't lie in any zbin
        # np.unique(photo_sample_zbin) -> [-1, 0, 1, 2, 3]
        sel *= photo_sample_zbin['zbin_mcal'] != -1

        # Cut photo_sample around COSMOS area to speed up matching
        arcmin = 10/60
        if ra is not None:
            print('Cutting photo_sample (RA)', flush=True)
            pra = photo_sample_sky[photo_columns_sky[0]]
            ra = np.asarray(ra)
            sel *= (pra >= ra.min() - arcmin) * (pra <= ra.max() + arcmin)

        if dec is not None:
            print('Cutting photo_sample (DEC)', flush=True)
            pdec = photo_sample_sky[photo_columns_sky[1]]
            dec = np.asarray(dec)
            sel *= (pdec >= dec.min() - arcmin) * (pdec <= dec.max() + arcmin)

        photo_sample = photo_sample[sel]
        photo_sample_sky = photo_sample_sky[sel]
        photo_sample_zbin = photo_sample_zbin[sel]

        # Add metacal response column
        # We approximate it to R = (R11 + R22) / 2
        R = (photo_sample_sky['R11'] + photo_sample_sky['R22']) / 2
        photo_sample = rfn.append_fields(photo_sample, 'R', R, usemask=False)

        print('Merging catalogs', flush=True)
        cat_photo = rfn.merge_arrays([photo_sample, photo_sample_sky, photo_sample_zbin], usemask=False, flatten=True)

        print('Computing magnitudes', flush=True)
        # Use magnitudes not fluxes
        for cm, cf in zip(photo_columns_mag, photo_columns_flux):
            sel = cat_photo[cf] > 0
            cat_photo = cat_photo[sel]
            cat_photo = rfn.append_fields(cat_photo, cm, np.log10(cat_photo[cf]))

        print('Writing file', flush=True)
        fitsio.write(path_photo_short, cat_photo)

    cat['cat'] = cat_photo
    return cat


def load_catalogs(catalog='COSMOS15', xmat_max_distance=1, only_column_names=False, full_sample=False, cosmo_patch=False, riz=False, xCell=False):
    if catalog not in ['COSMOS15', 'COSMOS15_less_cuts', 'PAU']:
        raise ValueError(f"Catalog {catalog} not implemented")

    cat = {}
    cat['photo'] = load_DES(None, None, None, only_column_names=only_column_names)
    cat['spec'] = co.load_spec_cat(catalog, only_column_names=only_column_names)

    if only_column_names:
        return cat

    # Load the sample to xmatch
    if full_sample:
        cat['photo'].update(load_DES(None, None, 'full_sample'))
    else:
        sra, sdec = DIR.columns_from_fits(cat['spec']['cat'], cat['spec']['columns_sky'])
        cat['photo'].update(load_DES(sra, sdec, catalog))

    fname_pxmat = f'/mnt/extraspace/gravityls_3/data/DESY1/DESY1wl_xmatch_with_{catalog}.fits'
    fname_sxmat = f'/mnt/extraspace/gravityls_3/data/DESY1/{catalog}_xmatch_with_DESY1wl.fits'

    if os.path.isfile(fname_pxmat):
        pcat_xmat = fitsio.read(fname_pxmat)
        scat_xmat = fitsio.read(fname_sxmat)
        cat['spec']['cat'] = scat_xmat
    else:
        cat['spec'] = co.load_spec_cat(catalog, only_column_names)

        print('Cross matching', flush=True)
        pcat_xmat, scat_xmat, pix_xmat, spec_ix_xmat = DIR.cross_match_photo_spec(cat['photo']['cat'],
                                                                          cat['spec']['cat'],
                                                                          cat['photo']['columns_sky'],
                                                                          cat['spec']['columns_sky'],
                                                                          return_ix_xmat=True,
                                                                          max_distance=xmat_max_distance)

        fitsio.write(fname_pxmat, pcat_xmat)
        fitsio.write(fname_sxmat, scat_xmat)

    # If cosmo_patch: use cosmology patch for DIR
    if cosmo_patch:
        cat['photo'].update(load_DES(None, [-90, -35], 'cosmology_patch', riz=riz, xCell=xCell))

    cat['photo']['cat_xmat'] = pcat_xmat
    cat['spec']['cat_xmat'] = scat_xmat

    return cat


def generate_sacc(Nz, fname, covng=False):
    print('Loading cls data sacc file', flush=True)
    path_des = '/mnt/extraspace/gravityls_3/S8z/Cls_new_pipeline/4096_DES_eBOSS_CMB/cls_covG_new.fits'
    s = sacc.Sacc.load_fits(path_des)

    for trs in s.get_tracer_combinations():
        if ('DESwl' not in trs[0]) or ('DESwl' not in trs[1]):
            s.remove_selection(tracers=trs)

    snew = sacc.Sacc()
    for i in range(4):
        trname = f'DESwl__{i}'
        Nzi, z_edges = Nz[i]
        z = (z_edges[:-1] + z_edges[1:]) / 2
        snew.add_tracer('NZ', trname, quantity=s.tracers[trname].quantity,
                        z=z, nz=Nzi)


    for dtype in s.get_data_types():
        for trs in s.get_tracer_combinations(data_type=dtype):
            ell, cl, ind = s.get_ell_cl(dtype, trs[0], trs[1], return_ind=True)
            bpw = s.get_bandpower_windows(ind)
            snew.add_ell_cl(dtype, trs[0], trs[1], ell, cl, window=bpw)

    covariance = s.covariance.covmat
    cov_suffix = 'G'
    if covng:
        path_des_ng = '/mnt/extraspace/gravityls_3/sacc_files/cls_noise_covNG_DESwl_Andrina_paper.fits'
        sng = sacc.Sacc.load_fits(path_des_ng)
        cov_suffix = 'GNG'
        covariance += sng.covariance.covmat

    snew.add_covariance(covariance)

    outname = f'{fname}_cls_cov{cov_suffix}_DIR.fits'
    print('Saving new file with only DESwl and DIR N(z)', flush=True)
    print(outname)
    snew.save_fits(outname, overwrite=True)


def main(fname, max_distance, nneigh, nbins, adjoint_colors=False,
         color_col=None, metacal_R=False, catalog='COSMOS15',
         colors='griz', full_sample=False, covng=False, cosmo_patch=False,
         xCell=False):
    Nz = []

    fname += f'_{catalog}'
    weights_column = None
    if metacal_R:
        weights_column = 'R'
        fname += '_R'

    fname += f'_maxd_{max_distance}_nngh_{nneigh}_{colors}'
    if full_sample and cosmo_patch:
        raise ValueError("full_sample and cosmo_patch cannot be simultaneously True")
    elif full_sample:
        fname += '_full_sample'
    elif cosmo_patch:
        fname += '_cosmo_patch'
    if xCell:
        fname += '_xCell'
    if adjoint_colors:
        fname += '_adjoint_colors'
    if color_col is not None:
        fname += f'_plus_{color_col}'
    fname_nz = fname + f'_nbins{nbins}'

    print(f"Checking for file {fname_nz}_bin0_Nz.npz")
    if not os.path.isfile(f'{fname_nz}_bin0_Nz.npz'):
        cat = load_catalogs(catalog, max_distance, full_sample=full_sample, cosmo_patch=cosmo_patch, riz=(colors == 'riz'), xCell=xCell)
        cat['photo']['columns_mag'] = [cm for cm in cat['photo']['columns_mag'] if cm.split('_')[-1] in colors]

        pcat_xmat = cat['photo']['cat_xmat']
        scat_xmat = cat['spec']['cat_xmat']

        print('Finished cross matching')
        for zbin in range(4):
            print(f'Selecting galaxies in zbin {zbin}', flush=True)
            # Based on https://github.com/xC-ell/xCell/blob/b00b448d18880afc3b405f939a7aabd934df45be/xcell/mappers/mapper_DESY1wl.py#L92-L96
            zcol = cat['photo']['z_column']
            sel = cat['photo']['cat'][zcol] == zbin
            sel_xmat = cat['photo']['cat_xmat'][zcol] == zbin

            print(f'Ngal xmatch bin {zbin}', np.sum(sel_xmat))
            print(f'Ngal photo bin {zbin}', np.sum(sel))
            print('doing DIR', flush=True)
            # zmax = 4 and nbins = 100 as in DES
            # Note, however, that there are some COSMOS outliers at z=10 that will
            # be ignored by setting zmax=4
            Nzi = DIR.do_DIR(cat['photo']['cat'][sel], pcat_xmat[sel_xmat], scat_xmat[sel_xmat],
                             cat['photo']['columns_mag'], cat['spec']['z_column'], nneigh=nneigh, zmax=4, nbins=nbins,
                             output=f'{fname_nz}_bin{zbin}', normalized_to_xmat=True,
                             adjoint_colors=adjoint_colors, color_col=color_col, weights_column=weights_column)
            Nz.append(Nzi)
    else:
        cat = load_catalogs(catalog, max_distance, only_column_names=True)
        cat['photo']['columns_mag'] = [cm for cm in cat['photo']['columns_mag'] if cm.split('_')[-1] in colors]
        for zbin in range(4):
            Nzi = DIR.do_DIR(None, None, None,
                             cat['photo']['columns_mag'], cat['spec']['z_column'],  nneigh=nneigh, zmax=4, nbins=nbins,
                             output=f'{fname_nz}_bin{zbin}', normalized_to_xmat=True,
                             adjoint_colors=adjoint_colors, color_col=color_col, weights_column=weights_column)
            Nz.append(Nzi)

    generate_sacc(Nz, fname_nz, covng)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Build the Nz for DES usin DIR with COSMOS15",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('output', type=str, help='Output file name')
    parser.add_argument('--max_distance', type=float, default=1,
                        help='Maximum distance for xmatch')
    parser.add_argument('--nneigh', type=int, default=20,
                        help='Number of nearest neighbors in color space.')
    parser.add_argument('--nbins', type=int, default=100,
                        help='Number of z-bins.')
    parser.add_argument('--adjoint_colors', action='store_true',
                        help='Use adjoint colors to calibrate the ' +
                        'N(z); i.e. g-r instead of g, r.')
    parser.add_argument('--color_col', type=str, default=None,
                        help='If adjoint_colors, use this magnitude color in the calibration too')
    parser.add_argument('--colors', type=str, default='griz',
                        help='Colors to use in the calibration.')
    parser.add_argument('--metacal_R', action='store_true',
                        help='If given, weight the Nz by the metacal weights R = (R11 + R22) / 2 (per galaxy)')
    parser.add_argument('--COSMOS15_less_cuts', action='store_true',
                        help='If given, use COSMOS15 but with less cuts')
    parser.add_argument('--COSMOS20', action='store_true',
                        help='If given, use COSMOS20 instead of COSMOS15')
    parser.add_argument('--PAU', action='store_true',
                        help='If given, use PAU instead of COSMOS15')
    parser.add_argument('--full_sample', action='store_true',
                        help='If given, use the DES full sample instead of a patch around the training sample')
    parser.add_argument('--covng', action='store_true',
                        help='If given, add the NG covariance to the sacc file')
    parser.add_argument('--cosmo_patch', action='store_true',
                        help='If given, only the cosmo patch will be used (-90 < dec < -34), plus the patch around the training sample')
    parser.add_argument('--xCell', action='store_true',
                        help='If given, use the riz catalog as in xCell.')

    args = parser.parse_args()

    if args.COSMOS20:
        cat = 'COSMOS20'
    elif args.PAU:
        cat = 'PAU'
    elif args.COSMOS15_less_cuts:
        cat = 'COSMOS15_less_cuts'
    else:
        cat = 'COSMOS15'

    if args.xCell and ((args.colors != 'riz') or (not args.cosmo_patch)):
        raise ValueError('--xCell needs --colors riz and --cosmo_patch.')

    main(args.output, args.max_distance, args.nneigh, args.nbins,
         args.adjoint_colors, args.color_col, metacal_R=args.metacal_R,
         catalog=cat, colors=args.colors, full_sample=args.full_sample,
         covng=args.covng, cosmo_patch=args.cosmo_patch, xCell=args.xCell)

