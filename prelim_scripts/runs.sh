#!/bin/bash

data=../DIR/output/cls_DES_covG_DIR.fits
queue=cmb
survey=DES

# Likelihoods
# All params
#addqueue -c cobaya_first_run -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params.yml
# No shifts
#addqueue -c cobaya_nopz -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_nopz.yml

# Compute derivative matrices
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p010.npz 0.01 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p005.npz 0.005 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p003.npz 0.003 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p002.npz 0.002 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p001.npz 0.001 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p5.npz 5 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 create_nz_response.py $data  output/T_${survey}_dnz0p5_2.npz 5 1
# python3 create_nz_response.py $data  output/T_${survey}_dnz0p5_2.npz 5 1

# Compute master CV covariance
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 covar_master.py $data output/covar_master_${survey}.npz 1 # 22.5

# Modified sacc files
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p010.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p010_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p005.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p005_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p003.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p003_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p002.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p002_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p001.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p001_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p5.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p5_diagx1.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p5.npz output/covar_master_${survey}.npz 1. output/cls_signal_covG_${survey}_marg0p5_diagx1.fits 1

# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p005.npz output/covar_master_${survey}.npz 2. output/cls_signal_covG_${survey}_marg0p005_diagx2.fits 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 /usr/bin/python3 modify_sacc.py $data output/T_${survey}_dnz0p005.npz output/covar_master_${survey}.npz 5. output/cls_signal_covG_${survey}_marg0p005_diagx5.fits 1

# Marginalized covariance runs
#addqueue -c cobaya_0p005_dx1 -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx1.yml
#addqueue -c cobaya_0p005_dx2 -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx2.yml
#addqueue -c cobaya_0p005_dx5 -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx5.yml
# addqueue -c cobaya_0p005_dx1_hsc -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx1_hsc.yml
# addqueue -c cobaya_0p005_dx2_hsc -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx2_hsc.yml
# addqueue -c cobaya_0p005_dx5_hsc -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_marg0p005_diagx5_hsc.yml

# HSC run
#addqueue -c cobaya_hsc_all -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_hsc_all.yml
# No shifts
#addqueue -c cobaya_hsc_nopz -O -n 2x12 -q $queue -m 1 /users/damonge/.local/bin/cobaya-run params_hsc_nopz.yml
