from scipy.special import jv
from scipy.interpolate import interp1d
from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
import pyccl as ccl
import sacc
import sys


fname_sacc = sys.argv[1]
fname_T = sys.argv[2]
fname_cov = sys.argv[3]
diag_factor = float(sys.argv[4])
fname_out = sys.argv[5]
resample = int(sys.argv[6])

s = sacc.Sacc.load_fits(fname_sacc)
names = list(s.tracers.keys())
nameids = {n: i for i, n in enumerate(names)}
nzs = []
norms = []
for n in names:
    # Downsampling a bit because it's ridiculous
    z = np.mean(s.tracers[n].z.reshape([-1, resample]), axis=1)
    dndz = np.mean(s.tracers[n].nz.reshape([-1, resample]), axis=1)
    # 93k galaxies in cosmos catalogs, 4 bins.
    # norm = 93000/(4*simps(dndz, x=z))
    norm = 1  # The histogrmas are already normalized to sum(Nz) = N_gal
    dndz *= norm
    dndz[dndz <= 0] = 1
    nzs.append((z, dndz))
    norms.append(norm)

t = np.load(fname_T)
Tmat = t['Tmat']
c = np.load(fname_cov)
cv_master = c['covar_master']

s.remove_selection(data_type='cl_eb')
s.remove_selection(data_type='cl_be')
s.remove_selection(data_type='cl_bb')

# Compute N(z) covariance
numz = len(cv_master)
cvN = np.zeros([4, numz, 4, numz])
for i1 in range(4):
    z1, nz1 = nzs[i1]
    norm1 = norms[i1]
    for i2 in range(4):
        z2, nz2 = nzs[i2]
        norm2 = norms[i2]
        cov_this = cv_master * nz1[:, None] * nz2[None, :]
        if i1 == i2:
            cov_this += np.diag(nz1)
        cov_this /= (norm1*norm2)
        cvN[i1, :, i2, :] = cov_this
cvN = cvN.reshape([4*numz, 4*numz])
# Double the diagonal
cvN += (diag_factor-1)*np.diag(np.diag(cvN))

covar_old = s.covariance.covmat
covar_new = covar_old + np.dot(Tmat.T, np.dot(cvN, Tmat))

s.add_covariance(covar_new)
s.save_fits(fname_out, overwrite=True)

plt.figure()
plt.imshow(Tmat)
plt.figure()
plt.imshow(covar_old / np.sqrt(np.diag(covar_old)[None, :] * np.diag(covar_old)[:, None]))
plt.colorbar()
plt.figure()
plt.imshow(covar_new / np.sqrt(np.diag(covar_new)[None, :] * np.diag(covar_new)[:, None]))
plt.colorbar()
plt.figure()
plt.imshow(covar_new / np.sqrt(np.diag(covar_new)[None, :] * np.diag(covar_new)[:, None])-
           covar_old / np.sqrt(np.diag(covar_old)[None, :] * np.diag(covar_old)[:, None]))
plt.colorbar()

plt.figure()
plt.imshow(covar_old-np.diag(np.diag(covar_old)))
plt.colorbar()
plt.figure()
plt.imshow(covar_new-np.diag(np.diag(covar_new)))
plt.colorbar()
plt.figure()
plt.imshow(covar_new-np.diag(np.diag(covar_new))-covar_old+np.diag(np.diag(covar_old)))
plt.colorbar()

plt.figure()
plt.plot(np.diag(covar_old), 'r-')
plt.plot(np.diag(covar_new), 'k-')
plt.show()
print(s.covariance.covmat.shape)
print(Tmat.shape)
print(cvN.shape)
