from scipy.interpolate import interp1d
from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
import pyccl as ccl
import sacc
import sys


fname_in = sys.argv[1]
fname_out = sys.argv[2]
dnz = float(sys.argv[3])
resample = int(sys.argv[4])

cosmo = ccl.CosmologyVanillaLCDM()

s = sacc.Sacc.load_fits(fname_in)
names = list(s.tracers.keys())
nameids = {n: i for i, n in enumerate(names)}
# Downsampling a bit because it's ridiculous
nzs = [(np.mean(s.tracers[n].z.reshape([-1, resample]), axis=1),
        np.mean(s.tracers[n].nz.reshape([-1, resample]) * (s.tracers[n].z[1] - s.tracers[n].z[0]), axis=1))
       for n in names]

l_sample = np.unique(np.geomspace(2., 31000., 400).astype(int)).astype(float)
l_sample = np.concatenate((np.array([0.]), l_sample))
bpws = {}
for n1, n2 in s.get_tracer_combinations():
    _, _, _, ind = s.get_ell_cl('cl_ee', n1, n2, return_cov=True, return_ind=True)
    bpw = s.get_bandpower_windows(ind)
    bpws[n1+'_'+n2] = (bpw.values, bpw.weight.T)


def get_cls(nzs):
    ts = [ccl.WeakLensingTracer(cosmo, nz) for nz in nzs]
    cls = []
    for n1, n2 in s.get_tracer_combinations():
        t1 = ts[nameids[n1]]
        t2 = ts[nameids[n2]]
        bpw = bpws[n1+'_'+n2]
        cl = ccl.angular_cl(cosmo, t1, t2, l_sample)
        f = interp1d(l_sample, cl)
        cl_unbinned = f(bpw[0])
        cl_binned = np.dot(bpw[1], cl_unbinned)
        cls.append(cl_binned)
    return np.array(cls)

Tmat = []
for i_n, n in enumerate(names):
    for iz, z in enumerate(nzs[i_n][0]):
        print(n, iz)
        # Add
        nzs[i_n][1][iz] += dnz
        clp = get_cls(nzs).flatten()
        # Subtract
        nzs[i_n][1][iz] -= 2*dnz
        clm = get_cls(nzs).flatten()
        # Restore and diff
        nzs[i_n][1][iz] += dnz
        Tmat.append((clp-clm)/(2*dnz))
Tmat = np.array(Tmat)
print(Tmat.shape)
np.savez(fname_out, Tmat=Tmat)
