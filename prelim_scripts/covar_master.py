from scipy.special import jv
from scipy.interpolate import interp1d
from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
import pyccl as ccl
import sacc
import sys


fname_in = sys.argv[1]
fname_out = sys.argv[2]
resample = int(sys.argv[3])
# mlim = float(sys.argv[4])

cosmo =ccl.CosmologyVanillaLCDM()

zs = np.linspace(0, 4, 1024)
# bz = (0.8346+(mlim-24)*(-0.0624))/(ccl.growth_factor(cosmo, 1./(1+zs)))**1.3
# bzi = interp1d(zs, bz)
def bzi(z):
    z = np.asarray(z)
    z_edges = [(0.2, 0.43), (0.43, 0.63),
               (0.63, 0.9), (0.9, 1.3)]
    bi = [0.7, 0.88, 1.10, 1.11]

    bz = np.ones_like(z)
    # Let's use the same bias for z<zbin as in th first bin
    bz[z<0.2] *= bi[0]
    for i, zei  in enumerate(z_edges):
        z_i, z_e = zei
        bz[(z >= z_i) * (z < z_e)] *= bi[i]

    return bz


print("Reading")
s = sacc.Sacc.load_fits(fname_in)
names = list(s.tracers.keys())
nameids = {n: i for i, n in enumerate(names)}
# Downsampling a bit because it's ridiculous
nzs = [(np.mean(s.tracers[n].z.reshape([-1, resample]), axis=1),
        np.mean(s.tracers[n].nz.reshape([-1, resample]), axis=1))
       for n in names]
print("Read")

area_deg2 = 2  # COSMOS30 area
area_rad2 = area_deg2*(np.pi/180)**2
theta_rad = np.sqrt(area_rad2/np.pi)
zz = nzs[0][0]
dz = np.mean(np.diff(zz))
z0 = zz-dz/2
zf = zz+dz/2
chi0 = ccl.comoving_radial_distance(cosmo, 1./(1+z0))
chif = ccl.comoving_radial_distance(cosmo, 1./(1+zf))
chi_m = 0.5*(chi0+chif)
dchi = chif-chi0
R_m = theta_rad*chi_m
b_m = bzi(zz)
f_m = ccl.growth_rate(cosmo,1./(1+zz))
n_kt = 512
n_kp = 512
kt_arr = np.geomspace(0.00005,10.,n_kt)
kp_arr = np.geomspace(0.00005,10.,n_kp)
k_arr = np.sqrt(kt_arr[None,:]**2+kp_arr[:,None]**2)
pk_arr = np.array([(b+f*kp_arr[:,None]**2/k_arr**2)**2*
                   ccl.nonlin_matter_power(cosmo,k_arr.flatten(),float(a)).reshape([n_kp,n_kt])
                   for a,b,f in zip(1./(1+zz),b_m,f_m)])
window = np.array([2*jv(1,kt_arr[None,:]*R)/(kt_arr[None,:]*R)*
                   np.sin(0.5*kp_arr[:,None]*dc)/(0.5*kp_arr[:,None]*dc)
                   for R,dc in zip(R_m,dchi)])


def covar_CV(i,j):
    # Windows
    win_i = window[i]
    win_j = window[j]
    # Power spectrum
    pk_ij = np.sqrt(pk_arr[i]*pk_arr[j])
    # Cosine term
    dchi_ij = chi_m[i]-chi_m[j]
    cos_ij = np.cos(kp_arr*dchi_ij)
    # First integrand
    integrand_pt = (kt_arr[None,:]**2*win_i*win_j*pk_ij)/(2*np.pi**2)
    # Integrate over k_transverse
    integrand_p = simps(integrand_pt,axis=1,x=np.log(kt_arr))*cos_ij*kp_arr
    # Integrate over k_parallel
    integral = simps(integrand_p,x = np.log(kp_arr))
    return integral


num_z = len(zz)
cv = np.zeros([num_z, num_z])
for i1 in range(num_z):
    print(i1)
    for i2 in range(i1, num_z):
        cv[i1, i2] = covar_CV(i1, i2)
        if i1 != i2:
            cv[i2, i1] = cv[i1, i2]

np.savez(fname_out, covar_master=cv)

plt.figure()
plt.imshow(cv)
plt.colorbar()

plt.figure()
plt.imshow(cv/np.sqrt(np.diag(cv)[:, None]*np.diag(cv)[None, :]))
plt.colorbar()

plt.figure()
for nz in nzs:
    plt.plot(nz[0], nz[1], 'k-')
    plt.errorbar(nz[0], nz[1], yerr=nz[1]*np.sqrt(np.diag(cv)), fmt='r.')
plt.show()
