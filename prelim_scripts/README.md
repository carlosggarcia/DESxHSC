# Horrible scripts

This folder contains a collection of scripts David used in the past to experiment with the N(z) marginalization for cosmic shear. I will refer to [this paper](https://arxiv.org/pdf/2007.14989.pdf) when explaining what each of these scripts do:

- `create_nz_response.py` computes the `T` matrix (derivatives of the power spectra w.r.t. the N(z) (eq. 2.17 of the paper).
- `covar_master.py` computes the cosmic variance part of the N(z) covariance (eq. 2.21 of the paper except for the N_i N_j factors, these are applied in `modify_sacc.py`). The logic behind the calculation is also explained in `COSMOS_CV.ipynb`.
- `modify_sacc.py` uses the products from the previous two scripts to generate the marginalized covariance for a given sacc file.

To estimate the N(z)s from an overlapping spectroscopic catalog we'll need to use the DIR method. An implementation of this can be found [here](https://github.com/nikfilippas/DIR_calibration) (see weights.py), or [here](https://github.com/LSSTDESC/DEHSC_LSS/blob/master/hsc_lss/cosmos_weight.py). It'd be best if you go through the code and create your own version, just to make sure you understand everything.
