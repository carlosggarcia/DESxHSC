#!/usr/bin/python
"""
Prior to be used with cobaya with import_module('prior_NzParam').get_NzParam_prior
"""
import numpy as np

mean = np.array([-0.01001383, 2.62571162])
cov = np.array([[1.30937472e-06, -6.54687359e-04], [-6.54687359e-04, 3.27343680e-01]])
icov = np.linalg.inv(cov)


def NzParam_prior_logp(clk_A_Nz, clk_alpha_Nz):
    # We use MCMC cov because bootstrap is very sensitive to some outliers in the DES sample
    # mean = HSC_bf =
    # cov = DES_cov_MCMC

    d = np.array([clk_A_Nz, clk_alpha_Nz]) - mean
    chi2 = d.dot(icov).dot(d)
    return -0.5 * chi2
