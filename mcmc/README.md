# Info
 - DES priors: https://github.com/carlosggarcia/montepython_public/blob/emilio/cl_cross_corr_params_v3/cl_cross_corr_v3_DES_K1000_all_mag.param (based on DESY1 paper)
 - HSC priors: https://arxiv.org/pdf/1809.09148.pdf

# Differences from MontePython likelihood:
The code yields virtually the same chi2 for the same model if one fixes the following:
 - Different l_sampling in interpolation
 - Interpolation in l, Cl, vs log(l), Cl
 - Different sign for dz
 - Different normalization of the amplitude in IA
 - gc-wl picks allways the largest lmin of both tracers (so you cannot start at `lmin=0` as in MP if you have `lmin!=0` for any of the tracers)
