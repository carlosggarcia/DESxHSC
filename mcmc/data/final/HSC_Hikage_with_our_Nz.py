import sacc
import numpy as np


def get_HSC_binning(lmin=60, lmax=6500, nbpw=15):
    edges = np.logspace(np.log10(lmin), np.log10(lmax), nbpw + 1)
    edges = np.round(edges)

    # binning is done by P_bl = \sum_{l\in b} (2*l + 1) * l^2
    ell_eff = np.zeros(nbpw)
    window = np.zeros((nbpw, lmax+1))
    i = 0
    for ilmin, ilmax in zip(edges[:-1], edges[1:]):
        ells = np.arange(ilmin, ilmax)
        norm = np.sum((2*ells + 1) * ells**2)
        wi = (2*ells + 1) * ells**2 / norm
        ell_eff[i] = np.sum(wi * ells)
        window[i, int(ilmin): int(ilmax)] = wi
        i += 1

    # Check window function
    ell_nobin = np.arange(6501)
    assert np.all(np.abs(window.dot(ell_nobin) / ell_eff -1) < 1e-10)

    return ell_nobin, ell_eff, window


# Create Sacc
sHikage = sacc.Sacc()

# Add tracers with our Nz
sOurs = sacc.Sacc.load_fits("./DESwl_final_HSC_cls_NzMarg_cov_p5.0_area2_diag4.0.fits")
for i in range(4):
    tracer = f'HSC__{i}'
    z = sOurs.tracers[tracer].z
    nz = sOurs.tracers[tracer].nz
    sHikage.add_tracer('NZ', tracer, quantity='galaxy_shear', spin=2, z=z, nz=nz)


# Add official Hikage data
# Columns are: ell | ell*(ell+1) * Cell / 2pi
bp = np.loadtxt('../HSC_official_Hikage/band_powers.dat', unpack=True)
ell = bp[0]

# Factor to recover the Cell
factor = ell * (ell+1) / (2 * np.pi)

# We use the binning operator as window, following Eq. 32 in 1809.09148
ell_nobin, ell2, window = get_HSC_binning()
# Check that the ell_eff are close (<1%) to the official ones
assert np.all(np.abs(ell2[5:11] / ell - 1) < 1e-2)

# Cut the matrix to have the same number of bins as the offical file
window = sacc.BandpowerWindow(ell_nobin, window[5:11, :].T)

c = 1
for i in range(4):
    for j in range(i, 4):
        sHikage.add_ell_cl('cl_ee', f'HSC__{i}', f'HSC__{j}', ell, bp[c] / factor, window=window)
        c += 1

# Add covariance
cov = np.loadtxt('../HSC_official_Hikage/cov_powers.dat')
# Divide by factor^2 to pass from the covariance of Dell -> Cell
cov_factor_block = factor[:, None] * factor[None, :]
cov_factor = np.block(10 * [10 * [cov_factor_block]])

sHikage.add_covariance(cov / cov_factor)

# Save Sacc
sHikage.save_fits('HSC_official_Hikage_Cells_with_our_Nz.fits')
