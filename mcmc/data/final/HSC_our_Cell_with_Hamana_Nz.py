import sacc
import numpy as np


# Create Sacc
sHamana = sacc.Sacc()

# Load our sacc
sOurs = sacc.Sacc.load_fits('./cls_DESwl_final_covGNG_HSC_covGNG_DIR.fits')
sOurs.keep_tracers([f'HSC__{i}' for i in range(4)])
sOurs.keep_selection(data_type='cl_ee')

# Add tracers with the official N(z). Those in Hamana+2020. They should be the same as in Hikage
# http://gfarm.ipmu.jp/~surhud/PDR1_HSCWL/Hamana2020/hsc16a_cstpcf_h2020.pz
# I think the difference with ours is just the binning
dndz = np.loadtxt('../HSC_official_Hamana2020/hsc16a_cstpcf_h2020.pz', unpack=True)
z = dndz[0]
assert z.size > 5

for i in range(4):
    tracer = f'HSC__{i}'
    sHamana.add_tracer('NZ', tracer, quantity='galaxy_shear', spin=2, z=z, nz=dndz[i+1])


# Add our Cell
for tr1, tr2 in sOurs.get_tracer_combinations():
    ell, cl, ix = sOurs.get_ell_cl('cl_ee', tr1, tr2, return_ind=True)
    window = sOurs.get_bandpower_windows(ix)
    sHamana.add_ell_cl('cl_ee', tr1, tr2, ell, cl, window=window)

# Add covariance
sHamana.add_covariance(sOurs.covariance.dense)

# Make sure things are ok
assert np.all(sOurs.mean == sHamana.mean)
assert np.all(sOurs.covariance.covmat == sHamana.covariance.covmat)

for i in range(4):
    tracer = f'HSC__{i}'
    tr = sHamana.get_tracer(tracer)
    assert np.all(tr.z == z)
    assert np.all(tr.nz == dndz[i+1])

# Save Sacc
sHamana.save_fits('HSC_our_Cell_with_Hamana_Nz.fits')
