#!/usr/bin/python
"""
Prior to be used with cobaya with import_module('prior_NzParamLinear').get_NzParamLinear_prior
"""
import numpy as np

mean = np.array([-0.00254, 0.0177])
sigma = np.array([0.0019, 0.0035])
corr = np.array([[1, -0.96], [-0.96, 1]])
cov = corr * sigma[:, None] * sigma[None, :]
icov = np.linalg.inv(cov)

icov_x2 = np.linalg.inv(cov * 2)
icov_x4 = np.linalg.inv(cov * 4)


def NzParamLinear_prior_logp(clk_A_Nz, clk_B_Nz):
    # Values obtained with http://localhost:8880/notebooks/codes/DESxHSC/DIR/PAU_COSMOS_comparison.ipynb
    # by David

    d = np.array([clk_A_Nz, clk_B_Nz]) - mean
    chi2 = d.dot(icov).dot(d)
    return -0.5 * chi2


def NzParamLinear_prior_logp_x2(clk_A_Nz, clk_B_Nz):
    # Values obtained with http://localhost:8880/notebooks/codes/DESxHSC/DIR/PAU_COSMOS_comparison.ipynb
    # by David

    d = np.array([clk_A_Nz, clk_B_Nz]) - mean
    chi2 = d.dot(icov_x2).dot(d)
    return -0.5 * chi2


def NzParamLinear_prior_logp_x4(clk_A_Nz, clk_B_Nz):
    # Values obtained with http://localhost:8880/notebooks/codes/DESxHSC/DIR/PAU_COSMOS_comparison.ipynb
    # by David

    d = np.array([clk_A_Nz, clk_B_Nz]) - mean
    chi2 = d.dot(icov_x4).dot(d)
    return -0.5 * chi2


def NzParamLinear_prior_logp_DESwl(clk_DESwl_A_Nz, clk_DESwl_B_Nz):
    return NzParamLinear_prior_logp(clk_DESwl_A_Nz, clk_DESwl_B_Nz)


def NzParamLinear_prior_logp_HSC(clk_HSC_A_Nz, clk_HSC_B_Nz):
    return NzParamLinear_prior_logp(clk_HSC_A_Nz, clk_HSC_B_Nz)
