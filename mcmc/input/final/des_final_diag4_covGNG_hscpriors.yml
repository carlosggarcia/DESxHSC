# DESwl with N(z) DIR-calibrated using COSMOS15 and 
# the full sample of DES. This has better <z> than 
# using the COSMOS field only
sampler:
  mcmc:
    burn_in: 1000
    max_tries: 1000

params:
  # List all the likelihood parameters here.
  # You need to include `prior`, `ref` and
  # `proposal` for all parameters you want
  # to vary.
  ln1e10A_s:
    prior:
      min: 1.5
      max: 6.
    latex: A_s
    proposal: 0.1
    drop: True

  omega_c:
    prior: 
      min: 0.03
      max: 0.7
    latex: \Omega_c
    proposal: 0.01
    drop: True

  omega_b:
    prior: 
      min: 0.019
      max: 0.026
    latex: \Omega_b
    proposal: 0.001
    drop: True

  h: 
    prior: 
      min: 0.6
      max: 0.9
    latex: h
    proposal: 0.02

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    latex: n_s
    proposal: 0.02

  m_nu: 0.

  Omega_c:
    value: 'lambda omega_c, h: omega_c / h**2'

  Omega_b:
    value: 'lambda omega_b, h: omega_b / h**2'

  A_sE9:
    value: 'lambda ln1e10A_s: np.exp(ln1e10A_s) * 0.1'

  sigma8:
    latex: \sigma_8

  # Bias parameters. Note that you can declare
  # all nuisance parameters as
  # `<prefix>_param_name` where `<prefix>` is
  # whatever you've chosen as `input_params_prefix`
  # further down. All such parameters will
  # automatically be understood by cobaya and
  # assigned to your likelihood.
  # First-order bias
  clk_DESwl__0_m:
    prior: 
      dist: norm
      loc: 0.012
      scale: 0.023
    latex: m^{DESwl}_0
    proposal: 0.005

  clk_DESwl__1_m: 
    prior: 
      dist: norm
      loc: 0.012
      scale: 0.023
    latex: m^{DESwl}_1
    proposal: 0.005

  clk_DESwl__2_m: 
    prior: 
      dist: norm
      loc: 0.012
      scale: 0.023
    latex: m^{DESwl}_2
    proposal: 0.005

  clk_DESwl__3_m:
    prior: 
      dist: norm
      loc: 0.012
      scale: 0.023
    latex: m^{DESwl}_3
    proposal: 0.005

  clk_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A_{IA}^{DESwl}
    proposal: 0.1

  clk_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta_{IA}^{DESwl}
    proposal: 0.1

# CCL settings
theory:
  cl_like.CCL:
    transfer_function: boltzmann_camb
    matter_pk: halofit
    baryons_pk: nobaryons

# Likelihood settings
likelihood:
  cl_like.ClLike:
    # Input sacc file
    input_file: data/final/DESwl_final_HSC_cls_NzMarg_cov_p5.0_area2_diag4.0.fits
    # List all relevant bins. The clustering
    # bins are clX, the shear bins are shX.
    bins:
      - name: DESwl__0
      - name: DESwl__1
      - name: DESwl__2
      - name: DESwl__3
    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      - bins: [DESwl__0, DESwl__0]
      - bins: [DESwl__0, DESwl__1]
      - bins: [DESwl__0, DESwl__2]
      - bins: [DESwl__0, DESwl__3]
      - bins: [DESwl__1, DESwl__1]
      - bins: [DESwl__1, DESwl__2]
      - bins: [DESwl__1, DESwl__3]
      - bins: [DESwl__2, DESwl__2]
      - bins: [DESwl__2, DESwl__3]
      - bins: [DESwl__3, DESwl__3]

    defaults:
      # Scale cut for galaxy clustering
      # (ignored for shear-shear)
      kmax: 0.15
      # These one will apply to all power
      # spectra (unless the lmax corresponding
      # to the chosen kmax is smaller).
      lmin: 0
      lmax: 2000
      DESwl__0:
        lmin: 30
      DESwl__1:
        lmin: 30
      DESwl__2:
        lmin: 30
      DESwl__3:
        lmin: 30

    # Prefix associated to all nuisance params
    input_params_prefix: clk
    ia_model: IADESY1
    nz_model: NzNone
    shape_model: ShapeMultiplicative
    # Linear, EulerianPT, LagrangianPT
    # bias_model: Linear
    bias_model: Linear

debug: True
output: 'chains/final/des_final_diag4_covGNG_hscpriors/des_final_diag4_covGNG_hscpriors'
