sampler:
  mcmc:
    burn_in: 1000
    max_tries: 100

params:
  # List all the likelihood parameters here.
  # You need to include `prior`, `ref` and
  # `proposal` for all parameters you want
  # to vary.
  A_sE9:
    prior:
      min: 0.5
      max: 5.
    ref:
      dist: norm
      loc: 2.02
      scale: 0.01
    latex: A_s
    proposal: 0.001

  Omega_c:
    prior: 
      min: 0.07
      max: 0.83
    ref:
      dist: norm
      loc: 0.2
      scale: 0.01
    latex: \Omega_c
    proposal: 0.01

  Omega_b:
    prior: 
      min: 0.03
      max: 0.07
    ref:
      dist: norm
      loc: 0.04
      scale: 0.001
    latex: \Omega_b
    proposal: 0.001

  h: 
    prior: 
      min: 0.55
      max: 0.91
    ref:
      dist: norm
      loc: 0.7
      scale: 0.02
    latex: h
    proposal: 0.02

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    ref:
      dist: norm
      loc: 0.96
      scale: 0.02
    latex: n_s
    proposal: 0.02

  m_nu: 0.

  sigma8:
    latex: \sigma_8

  # Bias parameters. Note that you can declare
  # all nuisance parameters as
  # `<prefix>_param_name` where `<prefix>` is
  # whatever you've chosen as `input_params_prefix`
  # further down. All such parameters will
  # automatically be understood by cobaya and
  # assigned to your likelihood.
  # First-order bias
  clk_wl_0_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_0
    proposal: 0.005

  clk_wl_1_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_1
    proposal: 0.005

  clk_wl_2_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_2
    proposal: 0.005

  clk_wl_3_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_3
    proposal: 0.005

  clk_wl_0_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0285
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: \Delta z^{HSC}_0
    proposal: 0.005

  clk_wl_1_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0135
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: \Delta z^{HSC}_1
    proposal: 0.005
    
  clk_wl_2_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0383
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: \Delta z^{HSC}_2
    proposal: 0.005

  clk_wl_3_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0376
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: \Delta z^{HSC}_3
    proposal: 0.005

  clk_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A_{IA}^{HSC}
    proposal: 0.1

  clk_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta_{IA}^{HSC}
    proposal: 0.1

# CCL settings
theory:
  cl_like.CCL:
    transfer_function: boltzmann_camb
    matter_pk: halofit
    baryons_pk: nobaryons

# Likelihood settings
likelihood:
  cl_like.ClLike:
    # Input sacc file
    input_file: /mnt/extraspace/gravityls_3/sacc_files/cls_signal_covGNG_HSC_Andrina_paper.fits
    # List all relevant bins. The clustering
    # bins are clX, the shear bins are shX.
    bins:
      - name: wl_0
      - name: wl_1
      - name: wl_2
      - name: wl_3
    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      - bins: [wl_0, wl_0]
      - bins: [wl_0, wl_1]
      - bins: [wl_0, wl_2]
      - bins: [wl_0, wl_3]
      - bins: [wl_1, wl_1]
      - bins: [wl_1, wl_2]
      - bins: [wl_1, wl_3]
      - bins: [wl_2, wl_2]
      - bins: [wl_2, wl_3]
      - bins: [wl_3, wl_3]

    defaults:
      # Scale cut for galaxy clustering
      # (ignored for shear-shear)
      kmax: 0.15
      # These one will apply to all power
      # spectra (unless the lmax corresponding
      # to the chosen kmax is smaller).
      lmin: 0
      lmax: 2000
      wl_0:
        lmin: 300
      wl_1:
        lmin: 300
      wl_2:
        lmin: 300
      wl_3:
        lmin: 300

    # Prefix associated to all nuisance params
    input_params_prefix: clk
    ia_model: IADESY1
    nz_model: NzShift
    shape_model: ShapeMultiplicative
    # Linear, EulerianPT, LagrangianPT
    # bias_model: Linear
    bias_model: Linear

debug: True
output: 'chains/hsc_dz_Andrina_ell300/hsc_dz_Andrina_ell300'
