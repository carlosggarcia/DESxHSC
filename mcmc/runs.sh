#!/bin/bash

queue=cmb

# Likelihoods

############
# Final run
###
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final old priors"     -o log/des_final_diag4_covGNG.out         ./launch_cobaya.sh input/final/des_final_diag4_covGNG.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final"     -o log/des_final_diag4_covGNG_hscpriors.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz final"     -o log/des_final_dz_covGNG_hscpriors.out             ./launch_cobaya.sh input/final/des_final_dz_covGNG_hscpriors.yml
#
# addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz final"     -o log/hsc_dz_hscpriors.out                          ./launch_cobaya.sh input/final/hsc_dz_hscpriors.yml
#
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final"     -o log/des_final_hsc_diag4_covGNG_hscpriors.out          ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors.yml

# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final indep"     -o log/des_final_hsc_diag4_covGNG_hscpriors_asindep.out        ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors_asindep.yml
# 
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final Param"     -o log/des_final_diag4_covGNG_NzParamLinear_hscpriors.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_NzParamLinear_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d4 Param"           -o log/hsc_diag4_NzParamLinear_hscpriors.out                       ./launch_cobaya.sh input/final/hsc_diag4_NzParamLinear_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final Param" -o log/des_final_hsc_diag4_covGNG_NzParamLinear_hscpriors.out      ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_NzParamLinear_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final Param px4" -o log/des_final_hsc_diag4_covGNG_NzParamLinear_priorx4_hscpriors.out      ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_NzParamLinear_priorx4_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final ParamPerSurvey" -o log/des_final_hsc_diag4_covGNG_NzParamLinearPerSurvey_hscpriors.out      ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_NzParamLinearPerSurvey_hscpriors.yml
#
# DES removing bins
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 012"     -o log/des_final_diag4_covGNG_hscpriors_012.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_012.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 013"     -o log/des_final_diag4_covGNG_hscpriors_013.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_013.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 023"     -o log/des_final_diag4_covGNG_hscpriors_023.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_023.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 123"     -o log/des_final_diag4_covGNG_hscpriors_123.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_123.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 01"     -o log/des_final_diag4_covGNG_hscpriors_01.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_01.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 final 23"     -o log/des_final_diag4_covGNG_hscpriors_23.out          ./launch_cobaya.sh input/final/des_final_diag4_covGNG_hscpriors_23.yml
#
# HSC with official Cells but our N(z)
# addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz official Cell our Nz"     -o log/hsc_dz_hscpriors_official_Cell_our_Nz.out                          ./launch_cobaya.sh input/final/hsc_dz_hscpriors_official_Cell_our_Nz.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz our Cell Hamana Nz"     -o log/hsc_dz_hscpriors_our_Cell_Hamana_Nz.out                          ./launch_cobaya.sh input/final/hsc_dz_hscpriors_our_Cell_Hamana_Nz.yml

# Forecast
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final forecast"     -o log/des_final_hsc_diag4_covGNG_hscpriors_forecast.out          ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors_forecast.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final forecast noNzMarg"     -o log/des_final_hsc_diag4_covGNG_hscpriors_forecast_noNzMarg.out          ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors_forecast_noNzMarg.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final forecast indep"     -o log/des_final_hsc_diag4_covGNG_hscpriors_forecast_asindep.out        ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors_forecast_asindep.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d4 final forecast Param"     -o log/des_final_hsc_diag4_covGNG_hscpriors_forecast_NzParamLinear.out          ./launch_cobaya.sh input/final/des_final_hsc_diag4_covGNG_hscpriors_forecast_NzParamLinear.yml

# Tests after referee comments (fixed DES bias and d4 -> d3)
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d3 final"     -o log/des_final_fixed_diag3_covGNG_hscpriors.out          ./launch_cobaya.sh input/final/des_final_fixed_diag3_covGNG_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES mlim d2 final"     -o log/des_final_mlim_diag2_covGNG_hscpriors.out          ./launch_cobaya.sh input/final/des_final_mlim_diag2_covGNG_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d3"     -o log/hsc_diag3_hscpriors.out                          ./launch_cobaya.sh input/final/hsc_diag3_hscpriors.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES HSC d3 final"     -o log/des_final_fixed_hsc_diag3_covGNG_hscpriors.out          ./launch_cobaya.sh input/final/des_final_fixed_hsc_diag3_covGNG_hscpriors.yml

############

############
# DES
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1"           -o log/des_diag1.out             ./launch_cobaya.sh input/des_diag1.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d2"           -o log/des_diag2.out             ./launch_cobaya.sh input/des_diag2.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz"           -o log/des_dz.out                ./launch_cobaya.sh input/des_dz.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz s8zpaper prior"           -o log/des_dz_s8zpaper_dzprior.out                ./launch_cobaya.sh input/des_dz_s8zpaper_dzprior.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz DIR"       -o log/des_dz_dir.out            ./launch_cobaya.sh input/des_dz_dir.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 NzPar"     -o log/des_diag1_NzParam.out     ./launch_cobaya.sh input/des_diag1_NzParam.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d2 NzPar"     -o log/des_diag2_NzParam.out     ./launch_cobaya.sh input/des_diag2_NzParam.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 NzParLin"  -o log/des_diag1_NzParamLinear.out     ./launch_cobaya.sh input/des_diag1_NzParamLinear.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b01"       -o log/des_diag1_bin01.out       ./launch_cobaya.sh input/des_diag1_bin01.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b012"      -o log/des_diag1_bin012.out      ./launch_cobaya.sh input/des_diag1_bin012.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b023"      -o log/des_diag1_bin023.out      ./launch_cobaya.sh input/des_diag1_bin023.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b123"      -o log/des_diag1_bin123.out      ./launch_cobaya.sh input/des_diag1_bin123.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b013"      -o log/des_diag1_bin013.out      ./launch_cobaya.sh input/des_diag1_bin013.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b2"        -o log/des_diag1_bin2.out        ./launch_cobaya.sh input/des_diag1_bin2.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 b3"        -o log/des_diag1_bin3.out        ./launch_cobaya.sh input/des_diag1_bin3.yml

#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz nbins20"   -o log/des_dz_nbins20.out        ./launch_cobaya.sh input/des_dz_nbins20.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz nbins30"   -o log/des_dz_nbins30.out        ./launch_cobaya.sh input/des_dz_nbins30.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz nbins40"   -o log/des_dz_nbins40.out        ./launch_cobaya.sh input/des_dz_nbins40.yml

# DES PAU calibration
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d4 PAU"       -o log/des_diag4_PAU.out         ./launch_cobaya.sh input/des_diag4_PAU.yml

# DES PAU full sample
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 PAU FS"       -o log/des_diag1_PAU_full_sample.out         ./launch_cobaya.sh input/des_diag1_PAU_full_sample.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 PAU FS R"     -o log/des_diag1_PAU_full_sample_metacal_R.out         ./launch_cobaya.sh input/des_diag1_PAU_full_sample_metacal_R.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 PAU FS R mlim" -o log/des_diag1_PAU_full_sample_metacal_R_mlim.out         ./launch_cobaya.sh input/des_diag1_PAU_full_sample_metacal_R_mlim.yml

# DES Shahab
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz Shahab"    -o log/des_dz_Shahab.out         ./launch_cobaya.sh input/des_dz_Shahab.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES Shahab"       -o log/des_diag1_Shahab.out      ./launch_cobaya.sh input/des_diag1_Shahab.yml

# DES Nz but shifted to match official <z>
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz shift"     -o log/des_dz_shift.out          ./launch_cobaya.sh input/des_dz_shift.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz shift 013"     -o log/des_dz_shift_013.out          ./launch_cobaya.sh input/des_dz_shift_013.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz shift 012"     -o log/des_dz_shift_012.out          ./launch_cobaya.sh input/des_dz_shift_012.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz shift 01"     -o log/des_dz_shift_01.out          ./launch_cobaya.sh input/des_dz_shift_01.yml

# DES Nz calibrated with the Full Sample
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample" -o log/des_diag1_full_sample.out ./launch_cobaya.sh input/des_diag1_full_sample.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4" -o log/des_diag4_full_sample.out ./launch_cobaya.sh input/des_diag4_full_sample.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4 covGNG" -o log/des_diag4_full_sample_covGNG.out ./launch_cobaya.sh input/des_diag4_full_sample_covGNG.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4 covGNG (hsc priors)" -o log/des_diag4_full_sample_covGNG_hscpriors.out ./launch_cobaya.sh input/des_diag4_full_sample_covGNG_hscpriors.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample dz DIR d4 covGNG" -o log/des_dz_full_sample_dir_diag4.out ./launch_cobaya.sh input/des_dz_full_sample_dir_diag4.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4 covGNG NzParamLinear" -o log/des_diag4_full_sample_covGNG_NzParamLinear.out ./launch_cobaya.sh input/des_diag4_full_sample_covGNG_NzParamLinear.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4 covGNG NzParamLinear x2" -o log/des_diag4_full_sample_covGNG_NzParamLinear_priorx2.out ./launch_cobaya.sh input/des_diag4_full_sample_covGNG_NzParamLinear_priorx2.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample d4 covGNG NzParamLinear x4" -o log/des_diag4_full_sample_covGNG_NzParamLinear_priorx4.out ./launch_cobaya.sh input/des_diag4_full_sample_covGNG_NzParamLinear_priorx4.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample_metacal_R" -o log/des_diag1_full_sample_metacal_R.out ./launch_cobaya.sh input/des_diag1_full_sample_metacal_R.yml
# addqueue -n 2x24 -s -q $queue -m 1  -c "DES full_sample_metacal_R_mlim" -o log/des_diag1_full_sample_metacal_R_mlim.out ./launch_cobaya.sh input/des_diag1_full_sample_metacal_R_mlim.yml

# HSC
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d1"           -o log/hsc_diag1.out             ./launch_cobaya.sh input/hsc_diag1.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d2"           -o log/hsc_diag2.out             ./launch_cobaya.sh input/hsc_diag2.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz"           -o log/hsc_dz.out                ./launch_cobaya.sh input/hsc_dz.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz DIR"       -o log/hsc_dz_dir.out            ./launch_cobaya.sh input/hsc_dz_dir.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz DIR d4"    -o log/hsc_dz_dir_diag4.out      ./launch_cobaya.sh input/hsc_dz_dir_diag4.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d1 NzPar"     -o log/hsc_diag1_NzParam.out     ./launch_cobaya.sh input/hsc_diag1_NzParam.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d2 NzPar"     -o log/hsc_diag2_NzParam.out     ./launch_cobaya.sh input/hsc_diag2_NzParam.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d1 NzParLin"  -o log/hsc_diag1_NzParamLinear.out     ./launch_cobaya.sh input/hsc_diag1_NzParamLinear.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d4 NzParLin"  -o log/hsc_diag4_NzParamLinear.out     ./launch_cobaya.sh input/hsc_diag4_NzParamLinear.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d4"           -o log/hsc_diag4.out             ./launch_cobaya.sh input/hsc_diag4.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d4 official priors"           -o log/hsc_diag4_official_priors.out             ./launch_cobaya.sh input/hsc_diag4_official_priors.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC d4 covGonly"   -o log/hsc_diag4_covGonly.out    ./launch_cobaya.sh input/hsc_diag4_covGonly.yml

# DESxHSC
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC d1"       -o log/des_hsc_diag1.out         ./launch_cobaya.sh input/des_hsc_diag1.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC d2"       -o log/des_hsc_diag2.out         ./launch_cobaya.sh input/des_hsc_diag2.yml

#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC indep d1" -o log/des_hsc_diag1_asindep.out ./launch_cobaya.sh input/des_hsc_diag1_asindep.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC indep d2" -o log/des_hsc_diag2_asindep.out ./launch_cobaya.sh input/des_hsc_diag2_asindep.yml

#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC d1 NzPar" -o log/des_hsc_diag1_NzParam.out ./launch_cobaya.sh input/des_hsc_diag1_NzParam.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESxHSC d2 NzPar" -o log/des_hsc_diag2_NzParam.out ./launch_cobaya.sh input/des_hsc_diag2_NzParam.yml

#addqueue -n 2x24 -s -q $queue -m 1  -c "DESfsxHSC d4 covGNG" -o log/des_fs_hsc_diag4_covGNG.out  ./launch_cobaya.sh input/des_fs_hsc_diag4_covGNG.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESfsxHSC d4 covGNG indep" -o log/des_fs_hsc_diag4_covGNG_asindep.out  ./launch_cobaya.sh input/des_fs_hsc_diag4_covGNG_asindep.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESfsxHSC d4 covGNG NzParamLinear" -o log/des_fs_hsc_diag4_covGNG_NzParamLinear.out  ./launch_cobaya.sh input/des_fs_hsc_diag4_covGNG_NzParamLinear.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESfsxHSC d4 covGNG NzParamLinearPerSurvey" -o log/des_fs_hsc_diag4_covGNG_NzParamLinearPerSurvey.out  ./launch_cobaya.sh input/des_fs_hsc_diag4_covGNG_NzParamLinearPerSurvey.yml

# Checks

# SD
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESgc x DESwl d1" -o log/desgc_deswl_diag1.out     ./launch_cobaya.sh input/desgc_deswl_diag1.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESgc x DESwl dz" -o log/desgc_deswl_dz.out     ./launch_cobaya.sh input/desgc_deswl_dz.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DESgc x DESwl dz (S8z)" -o log/desgc_deswl_dz_s8zpaper.out     ./launch_cobaya.sh input/desgc_deswl_dz_s8zpaper.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "SD d1"            -o log/SD_diag1.out              ./launch_cobaya.sh input/SD_diag1.yml


###
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (david)"  -o log/hsc_dz_davidsaccfile.out       ./launch_cobaya.sh input/hsc_dz_davidsaccfile.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (Andrina covG)"  -o log/hsc_dz_Andrina_covGonly.out       ./launch_cobaya.sh input/hsc_dz_Andrina_covGonly.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (300)"  -o log/hsc_dz_ell300.out       ./launch_cobaya.sh input/hsc_dz_ell300.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (Andrina 300)"  -o log/hsc_dz_Andrina_ell300.out       ./launch_cobaya.sh input/hsc_dz_Andrina_ell300.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (Andrina covG 300)"  -o log/hsc_dz_Andrina_covGonly_ell300.out       ./launch_cobaya.sh input/hsc_dz_Andrina_covGonly_ell300.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "HSC dz (Andrina)"  -o log/hsc_dz_Andrina.out       ./launch_cobaya.sh input/hsc_dz_Andrina.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES d1 (FD)"     -o log/des_diag1_FD.out       ./launch_cobaya.sh input/des_diag1_FD.yml
#addqueue -n 2x24 -s -q $queue -m 1  -c "DES dz (S8z)"     -o log/des_dz_s8zpaper.out       ./launch_cobaya.sh input/des_dz_s8zpaper.yml

