# DESxHSC

Cosmological constraints with DES and HSC data.

Quick references:
 - HSC:
   - Cosmic shear (Official): https://arxiv.org/pdf/1809.09148.pdf
   - Cosmic shear official likelihood: https://github.com/chiaki-hikage/Likelihood_pseudoCl_HSCY1
   - Galaxy clustering (Andrina): https://arxiv.org/pdf/1912.08209.pdf
 - DES:
   - Weak lensing N(z): https://arxiv.org/pdf/1708.01532.pdf
 - N(z) marginalization: https://arxiv.org/pdf/2007.14989.pdf
 - bias(mag_lim): https://arxiv.org/pdf/1912.08209.pdf
 - Systematics: 
   - COSMOS15: https://arxiv.org/pdf/1604.02350.pdf
   - PAU: https://arxiv.org/pdf/2007.11132.pdf (Fig. 5)
   - COSMOS20: https://arxiv.org/pdf/2110.13923.pdf

# Reference
```
@ARTICLE{2023JCAP...01..025G,
       author = {{Garc{\'\i}a-Garc{\'\i}a}, Carlos and {Alonso}, David and {Ferreira}, Pedro G. and {Hadzhiyska}, Boryana and {Nicola}, Andrina and {S{\'a}nchez}, Carles and {Slosar}, An{\v{z}}e},
        title = "{Combining cosmic shear data with correlated photo-z uncertainties: constraints from DESY1 and HSC-DR1}",
      journal = {\jcap},
     keywords = {cosmological parameters from LSS, power spectrum, redshift surveys, weak gravitational lensing, Astrophysics - Cosmology and Nongalactic Astrophysics},
         year = 2023,
        month = jan,
       volume = {2023},
       number = {1},
          eid = {025},
        pages = {025},
          doi = {10.1088/1475-7516/2023/01/025},
archivePrefix = {arXiv},
       eprint = {2210.13434},
 primaryClass = {astro-ph.CO},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2023JCAP...01..025G},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```
