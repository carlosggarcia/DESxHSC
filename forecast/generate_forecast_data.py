#!/usr/bin/python3
import sacc
import numpy as np

sacc_file = sacc.Sacc.load_fits('../nz_marginalization/data/cls_DESwl_final_covGNG_HSC_covGNG_DIR.fits')
sacc_file.remove_selection(data_type='cl_0b')
sacc_file.remove_selection(data_type='cl_eb')
sacc_file.remove_selection(data_type='cl_be')
sacc_file.remove_selection(data_type='cl_bb')

# Modify DES covariance first since the BF sacc file has scale cuts applied
# LSST ~ 10k deg^2
# DES ~ 1k deg^2
# HSC ~ 100 deg^2
factor_des = 10
factor_hsc = 100

ixDES = []
ixHSC = []
for trs in sacc_file.get_tracer_combinations():
    if ('DES' in trs[0]) and ('DES' in trs[1]):
        ixDES.extend(sacc_file.indices(tracers=trs))
    elif ('HSC' in trs[0]) and ('HSC' in trs[1]):
        ixHSC.extend(sacc_file.indices(tracers=trs))
    else:
        raise ValueError(f'Something went wrong. You have tracers {trs} in the sacc file')

# Way 1
cov = sacc_file.covariance.covmat.copy()
factor_mat = np.ones_like(cov)
factor_mat[np.ix_(ixDES, ixDES)] /= factor_des
factor_mat[np.ix_(ixHSC, ixHSC)] /= factor_hsc
cov = cov * factor_mat

# Way 2
sacc_file.covariance.covmat[np.ix_(ixDES, ixDES)] /= factor_des
sacc_file.covariance.covmat[np.ix_(ixHSC, ixHSC)] /= factor_hsc

print('Max abs reldev between way1 and way2', np.max(np.abs((cov + 1e-100) / (sacc_file.covariance.covmat + 1e-100) - 1)))
# Asserting that they are both the same seems to be too strong so just impose a
# reldev ridiculously low
# assert np.all(cov == sacc_file.covariance.covmat)
assert np.max(np.abs((cov + 1e-100) / (sacc_file.covariance.covmat + 1e-100) - 1)) < 1e-10


# Now add the Nz marginalized covariance (i.e. TPT)
cov_nz = np.load('../nz_marginalization/output/DESwl_final_HSC/DESwl_final_HSC_covNzMarg_p5.0_diag4.0.npz')['nz_marg']
sacc_file_nz = sacc_file.copy()
sacc_file_nz.add_covariance(sacc_file.covariance.covmat + cov_nz)


# Add BF Cells
sBF = sacc.Sacc.load_fits('../mcmc/chains/final/des_final_hsc_diag4_covGNG_hscpriors/cls_cov_bf.fits')
for trs in sBF.get_tracer_combinations():
    ell, _ = sBF.get_ell_cl('cl_ee', trs[0], trs[1])
    sacc_file.remove_selection(data_type='cl_ee', tracers=trs, ell__lt=ell[0])
    sacc_file.remove_selection(data_type='cl_ee', tracers=trs, ell__gt=ell[-1])
    sacc_file_nz.remove_selection(data_type='cl_ee', tracers=trs, ell__lt=ell[0])
    sacc_file_nz.remove_selection(data_type='cl_ee', tracers=trs, ell__gt=ell[-1])
    ell2, _ = sacc_file.get_ell_cl('cl_ee', trs[0], trs[1])
    assert np.all(ell == ell2)
    ell2, _ = sacc_file_nz.get_ell_cl('cl_ee', trs[0], trs[1])
    assert np.all(ell == ell2)

# Check both sacc files have the same ordering before modifying the data vector
assert sacc_file.get_tracer_combinations() == sBF.get_tracer_combinations()
assert sacc_file_nz.get_tracer_combinations() == sBF.get_tracer_combinations()
sacc_file.mean = sBF.mean.copy()
sacc_file_nz.mean = sBF.mean.copy()

# Save the files
sacc_file.save_fits('cls_DESwl_final_covGNG_HSC_covGNG_DIR_forecast.fits', overwrite=True)
sacc_file_nz.save_fits('cls_DESwl_final_covGNG_HSC_covGNG_DIR_forecast_covNzMarg_p5.0_diag4.0.fits',
                       overwrite=True)


# Create "as independent" case (i.e. remove correlations between DES & HSC)
sacc_file = None  # Erase sacc_file with no NzMarg to avoid using it by error
ixDES = []
ixHSC = []
for trs in sacc_file_nz.get_tracer_combinations():
    if ('DES' in trs[0]) and ('DES' in trs[1]):
        ixDES.extend(sacc_file_nz.indices(tracers=trs))
    elif ('HSC' in trs[0]) and ('HSC' in trs[1]):
        ixHSC.extend(sacc_file_nz.indices(tracers=trs))
    else:
        raise ValueError(f'Something went wrong. You have tracers {trs} in the sacc file')


cov_asindep = np.zeros_like(sacc_file_nz.covariance.covmat)
cov_asindep[np.ix_(ixDES, ixDES)] = sacc_file_nz.covariance.covmat[np.ix_(ixDES, ixDES)]
cov_asindep[np.ix_(ixHSC, ixHSC)] = sacc_file_nz.covariance.covmat[np.ix_(ixHSC, ixHSC)]
sacc_file_nz.covariance.covmat = cov_asindep

sacc_file_nz.save_fits('cls_DESwl_final_covGNG_HSC_covGNG_DIR_forecast_covNzMarg_p5.0_diag4.0_asindependent.fits',
                       overwrite=True)
