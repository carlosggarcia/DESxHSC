#!/usr/bin/python3

from glob import glob
from matplotlib import pyplot as plt
import os
import numpy as np

def load_files(output):
    bname = os.path.basename(output)
    fname = os.path.join(output, bname +  '_covNzMarg_p*.npz')
    files = glob(fname)
    covs = np.array([np.load(f)['nz_marg'] for f in files])
    labels = np.array([float(f.split('_')[-2].replace('p', '')) for f in files])

    ix = np.argsort(labels)

    return covs[ix], labels[ix]


def plot_cov_diagonal(covariances, labels):
    for c, l in zip(covariances, labels):
        plt.semilogy(np.diag(c), label=l)
    plt.legend(loc=0)
    plt.show()
    plt.close()


def plot_cov_row(covariances, labels, row):
    for c, l in zip(covariances, labels):
        plt.semilogy(c[row], label=l)
    plt.legend(loc=0)
    plt.show()
    plt.close()


def main(output, row=None):
    print('Loading files')
    covs, labels = load_files(output)
    print('Files loaded')

    if row is None:
        print('Plotting diagonal of the TPT covariance')
        plot_cov_diagonal(covs, labels)
    else:
        print(f'Plotting row {row} of the TPT covariance')
        plot_cov_row(covs, labels, row)

    print('Done')


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Check the TPT^t convergence for different dnz outputs")
    parser.add_argument('output', type=str, help='Path to the folder where the covariances are stored (assumes it is called the same as the prefix of the NzMarg files)')
    parser.add_argument('--row', '-r', type=int, help='Row to plot instead of diagonal')

    args = parser.parse_args()

    main(args.output, args.row)
