# Warning

`nz_marginalization` assumes that the N(z) in the sacc file are normalized so
that `sum(nz * dz) = Ngal` with `dz` constant.

The `Tmat` from `nz_marginalization.py` and
`prelim_scripts/create_nz_response.py` are not comparable for the same `dz`.
This is because the former normalizes the `nz` so that `sum(nz) = Ngal`;
whereas the latter just uses the `nz` that is found in the sacc file

