#!/usr/bin/python3

from scipy.special import jv
from scipy.interpolate import interp1d
from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
import pyccl as ccl
import sacc
import sys
import os

class NzMarg():
    def __init__(self, sfile, dnz, bzi, area_deg2=2, diag_factor=1,
                 resample=None, outdir='./', outprefix='', dir_tracers=None):
        self._cosmo = None
        self.sfile = s = sfile
        self.dnz = dnz
        self.outprefix = os.path.join(outdir, outprefix)
        self.resample = resample
        self.bzi = bzi
        self.area_deg2 = area_deg2
        self.diag_factor = diag_factor

        # self.names will contain only the tracers for which we will marginalized over
        # and self.nameids the index for their position in self.nzs
        self.names, self.nameids, self.nzs = self._get_names_namesids_nzs(dir_tracers)

        # The following has to be called after defining self.names and
        # self.dir_tracers
        self.dir_surveys = self._get_dir_surveys()

        s.remove_selection(data_type='cl_0b')
        s.remove_selection(data_type='cl_eb')
        s.remove_selection(data_type='cl_be')
        s.remove_selection(data_type='cl_bb')

        self.Tmat = None
        self.cv = None
        self.nz_marg = None

        self._base_ccl_tr = None
        self._base_cls_arr = None
        os.makedirs(outdir, exist_ok=True)

    def _get_names_namesids_nzs(self, dir_tracers):
        s = self.sfile
        resample = self.resample

        if dir_tracers is None:
            names = list(s.tracers.keys())
        else:
            names = dir_tracers

        nzs = []
        for n in names:
            tr = s.tracers[n]
            if not isinstance(tr, sacc.tracers.NZTracer):
                names.remove(n)
            z = tr.z
            # This assumes N(z) normalized s.t. \sum_i nz_i dz_i = Ngal and
            # that dz_i = const
            nz = tr.nz * (z[1] - z[0])
            if resample:
                z = np.mean(z.reshape([-1, resample]), axis=1)
                nz = np.mean(nz.reshape([-1, resample]), axis=1)

            nzs.append((z, nz))

        nameids = {n: i for i, n in enumerate(names)}

        return names, nameids, nzs

    def _get_dir_surveys(self):
        surveys = []
        for n in self.names:
            survey = self._get_survey(n)
            if survey not in surveys:
                surveys.append(survey)

        return surveys

    def get_cosmo(self):
        if self._cosmo is None:
            self._cosmo = ccl.CosmologyVanillaLCDM()
        return self._cosmo

    def _get_dtype(self, tr1, tr2):
        q1 = self.sfile.tracers[tr1].quantity
        q2 = self.sfile.tracers[tr2].quantity
        dtype = 'cl_'

        for qi in [q1, q2]:
            if qi in ['galaxy_density', 'cmb_convergence']:
                dtype += '0'
            elif qi in 'galaxy_shear':
                dtype += 'e'
            else:
                raise ValueError('tracer type not implemented:', qi)

        if dtype == 'cl_e0':
            dtype = 'cl_0e'

        return dtype

    def _get_ccl_tracer(self, trname, nz=None):
        tr = self.sfile.get_tracer(trname)

        cosmo = self.get_cosmo()
        if (nz is None) and isinstance(tr, sacc.tracers.NZTracer):
            nz = tr.nz

        q = tr.quantity
        if q == 'galaxy_density':
            bias = (tr.z, self.bzi[trname](tr.z))
            tr_ccl = ccl.NumberCountsTracer(cosmo, dndz=(tr.z, nz), bias=bias, has_rsd=False)
        elif q == 'galaxy_shear':
            tr_ccl = ccl.WeakLensingTracer(cosmo, (tr.z, nz))
        elif q == 'cmb_convergence':
            tr_ccl = ccl.CMBLensingTracer(cosmo, z_source=1100)

        return tr_ccl

    def _get_base_ccl_tracers(self):
        if self._base_ccl_tr is None:
            self._base_ccl_tr = {trn: self._get_ccl_tracer(trn) for trn in self.sfile.tracers.keys()}
        return self._base_ccl_tr

    def _get_base_cls_array(self):
        if self._base_cls_arr is None:
            ts = self._get_base_ccl_tracers()

            cls = []
            for n1, n2 in self.sfile.get_tracer_combinations():
                t1 = ts[n1]
                t2 = ts[n2]
                cl_binned = self._get_binned_cell(n1, n2, t1, t2)
                cls.append(cl_binned)
            self._base_cls_arr = np.array(cls)

        return self._base_cls_arr

    def _get_binned_cell(self, trn1, trn2, tr1, tr2):
        cosmo = self.get_cosmo()
        l_sample = np.unique(np.geomspace(2., 31000., 400).astype(int)).astype(float)
        l_sample = np.concatenate((np.array([0.]), l_sample))

        dtype = self._get_dtype(trn1, trn2)
        ind = self.sfile.indices(dtype, (trn1, trn2))
        bpw = self.sfile.get_bandpower_windows(ind)

        cl = ccl.angular_cl(cosmo, tr1, tr2, l_sample)
        cl_unbinned = interp1d(l_sample, cl)(bpw.values)
        cl_binned = np.dot(bpw.weight.T, cl_unbinned)

        return cl_binned

    def compute_nz_response(self):
        dnz = self.dnz
        fname = self.outprefix + f'_T_p{dnz}.npz'

        if self.Tmat is not None:
            pass
        elif os.path.isfile(fname):
            self.Tmat = np.load(fname)['Tmat']
        else:
            # Copied from prelim_script/create_nz_response.py
            cosmo = self.get_cosmo()
            s = self.sfile
            names, nameids, nzs = self.names, self.nameids, self.nzs

            base_ccl_tr =  self._get_base_ccl_tracers()
            base_cls_arr = self._get_base_cls_array()

            def get_cls(name, nz):
                cls = base_cls_arr.copy()
                for i, n1n2 in enumerate(s.get_tracer_combinations()):
                    n1, n2 = n1n2
                    print(n1, n2, flush=True)
                    if (n1 != name) and (n2 != name):
                        continue

                    if n1 == name:
                        t1 = self._get_ccl_tracer(n1, nz)
                    else:
                        t1 = base_ccl_tr[n1]

                    if n2 == name:
                        t2 = self._get_ccl_tracer(n2, nz)
                    else:
                        t2 = base_ccl_tr[n2]

                    cl_binned = self._get_binned_cell(n1, n2, t1, t2)
                    cls[i] = cl_binned
                return np.concatenate(cls)

            Tmat = []
            for i_n, n in enumerate(names):
                for iz, z in enumerate(nzs[i_n][0]):
                    print(n, iz)
                    nz = nzs[i_n][1].copy()
                    # Add
                    nz[iz] += dnz
                    clp = get_cls(n, nz)
                    # Subtract
                    nz[iz] -= 2 * dnz
                    clm = get_cls(n, nz)
                    Tmat.append((clp-clm)/(2*dnz))
            self.Tmat = np.array(Tmat)
            print(self.Tmat.shape)
            np.savez(fname, Tmat=self.Tmat)

        return self.Tmat

    def _compute_covar_master(self, bzi1, bzi2=None):
        # Copied from prelim_script/covar_master.py
        cosmo = self.get_cosmo()
        s = self.sfile

        if bzi2 is None:
            bzi2 = bzi1

        # TODO: This also should depend on the survey by the overlapping area.
        # Negligible, though
        area_rad2 = self.area_deg2*(np.pi/180)**2
        theta_rad = np.sqrt(area_rad2/np.pi)
        # TODO: This assumes same binning for both surveys
        zz = self.nzs[0][0]
        dz = np.mean(np.diff(zz))
        z0 = zz-dz/2
        zf = zz+dz/2
        # Sometimes due to numerical errors one can have z0[0] ~ -10e-10
        z0[z0<0] = 0
        chi0 = ccl.comoving_radial_distance(cosmo, 1./(1+z0))
        chif = ccl.comoving_radial_distance(cosmo, 1./(1+zf))
        chi_m = 0.5*(chi0+chif)
        dchi = chif-chi0
        R_m = theta_rad*chi_m
        b1_m = bzi1(zz)
        b2_m = bzi2(zz)
        f_m = ccl.growth_rate(cosmo,1./(1+zz))
        n_kt = 512
        n_kp = 512
        kt_arr = np.geomspace(0.00005,10.,n_kt)
        kp_arr = np.geomspace(0.00005,10.,n_kp)
        k_arr = np.sqrt(kt_arr[None,:]**2+kp_arr[:,None]**2)
        pk_arr = np.array([(b1+f*kp_arr[:,None]**2/k_arr**2)*(b2+f*kp_arr[:,None]**2/k_arr**2)*
                           ccl.nonlin_matter_power(cosmo,k_arr.flatten(),float(a)).reshape([n_kp,n_kt])
                           for a,b1,b2,f in zip(1./(1+zz),b1_m,b2_m,f_m)])
        # TODO: This also should depend on the survey by the overlapping area.
        # Negligible, though
        window = np.array([2*jv(1,kt_arr[None,:]*R)/(kt_arr[None,:]*R)*
                           np.sin(0.5*kp_arr[:,None]*dc)/(0.5*kp_arr[:,None]*dc)
                           for R,dc in zip(R_m,dchi)])


        def covar_CV(i,j):
            """
            i (int): index for z_i element
            j (int): index for z_j element
            """
            # Windows
            win_i = window[i]
            win_j = window[j]
            # Power spectrum
            pk_ij = np.sqrt(pk_arr[i]*pk_arr[j])
            # Cosine term
            dchi_ij = chi_m[i]-chi_m[j]
            cos_ij = np.cos(kp_arr*dchi_ij)
            # First integrand
            integrand_pt = (kt_arr[None,:]**2*win_i*win_j*pk_ij)/(2*np.pi**2)
            # Integrate over k_transverse
            integrand_p = simps(integrand_pt,axis=1,x=np.log(kt_arr))*cos_ij*kp_arr
            # Integrate over k_parallel
            integral = simps(integrand_p,x = np.log(kp_arr))
            return integral


        num_z = len(zz)
        cv = np.zeros([num_z, num_z])
        for i1 in range(num_z):
            print(i1)
            for i2 in range(i1, num_z):
                cv[i1, i2] = covar_CV(i1, i2)
                if i1 != i2:
                    cv[i2, i1] = cv[i1, i2]
        return cv

    def compute_covar_master(self):
        fname = self.outprefix + f'_covar_master_area{self.area_deg2}.npz'

        if self.cv is not None:
            pass
        elif os.path.isfile(fname):
            self.cv = np.load(fname)
        else:
            cv = {}
            for i, s1 in enumerate(self.dir_surveys):
                for s2 in self.dir_surveys[i:]:
                    bzi1 = self.bzi[s1]
                    bzi2 = self.bzi[s2]
                    cv[f'{s1}x{s2}'] = self._compute_covar_master(bzi1, bzi2)

            np.savez(fname, **cv)
            self.cv = cv

        return self.cv

    def compute_nz_covar(self):
        fname = self.outprefix + f'_covNzMarg_p{self.dnz}_diag{self.diag_factor}.npz'

        if self.nz_marg is not None:
            pass
        elif os.path.isfile(fname):
            self.nz_marg = np.load(fname)['nz_marg']
        else:
            Tmat = self.compute_nz_response()
            cv_master = self.compute_covar_master()

            # Compute N(z) covariance
            numz = self.nzs[0][0].size
            numnames = len(self.names)
            cvN = np.zeros([numnames, numz, numnames, numz])
            for i1, name1 in enumerate(self.names):
                z1, nz1 = self.nzs[i1]
                survey1 = self._get_survey(name1)
                for i2, name2 in enumerate(self.names):
                    z2, nz2 = self.nzs[i2]
                    survey2 = self._get_survey(name2)
                    if i1 <= i2:
                        cov_this = cv_master[f'{survey1}x{survey2}'] * nz1[:, None] * nz2[None, :]
                    else:
                        cov_this = cv_master[f'{survey2}x{survey1}'] * nz1[:, None] * nz2[None, :]
                    if i1 == i2:
                        cov_this += np.diag(nz1)
                    cvN[i1, :, i2, :] = cov_this
            cvN = cvN.reshape([numnames*numz, numnames*numz])
            # Double the diagonal
            cvN += (self.diag_factor-1)*np.diag(np.diag(cvN))

            self.nz_marg = np.dot(Tmat.T, np.dot(cvN, Tmat))
            np.savez(fname, nz_marg=self.nz_marg, cov_nz=cvN)

        return self.nz_marg

    def write_sacc(self):
        nz_marg = self.compute_nz_covar()

        # Use dense instead of covmat for compatibility with BlockDiagonal covs
        covar_old = self.sfile.covariance.dense
        covar_new = covar_old + nz_marg

        self.sfile.add_covariance(covar_new)
        fname = self.outprefix + f'_cls_NzMarg_cov_p{self.dnz}_area{self.area_deg2}_diag{self.diag_factor}.fits'
        self.sfile.save_fits(fname, overwrite=True)

        return covar_new

    def _get_survey(self, name):
        return name.split('__')[0]

