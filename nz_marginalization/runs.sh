#!/bin/bash

queue=cmb

# Compute derivative matrices
# DES = DESwl + DESgc
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_0.5.out /usr/bin/python3 NzMarg_data.py --des -dnz 0.5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_0.7.out /usr/bin/python3 NzMarg_data.py --des -dnz 0.7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_1.out   /usr/bin/python3 NzMarg_data.py --des -dnz 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_2.out   /usr/bin/python3 NzMarg_data.py --des -dnz 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_5.out   /usr/bin/python3 NzMarg_data.py --des -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_7.out   /usr/bin/python3 NzMarg_data.py --des -dnz 7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_10.out  /usr/bin/python3 NzMarg_data.py --des -dnz 10
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_15.out  /usr/bin/python3 NzMarg_data.py --des -dnz 15

# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/des_5_df2.out   /usr/bin/python3 NzMarg_data.py --des -dnz 5 -df 2

# DESwl + HSC
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_1.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_2.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_5.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_7.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_10.out  /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 10

# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_5_df2.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 5 -df 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_hsc_5_df4.out   /usr/bin/python3 NzMarg_data.py --deswl --hsc -dnz 5 -df 4
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_fs_hsc_5_df4.out   /usr/bin/python3 NzMarg_data.py --deswl_full_sample --hsc -dnz 5 -df 4

# DESwl
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_5.out    /usr/bin/python3 NzMarg_data.py --deswl -dnz 5

# DESwl full sample
# Test convergence of the numerical derivative
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_1.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_2.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_5.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_7.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_10.out   /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 10
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_15.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 15
#
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_5_df4.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample -dnz 5 -df 4
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_metacal_R_5.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample_metacal_R -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_full_sample_metacal_R_mlim_5.out    /usr/bin/python3 NzMarg_data.py --deswl_full_sample_metacal_R_mlim -dnz 5

# DESwl Shahab Nz
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_shahab_5.out    /usr/bin/python3 NzMarg_data.py --shahab -dnz 5

# DESwl PAU
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_PAU_5_df4.out    /usr/bin/python3 NzMarg_data.py --deswl_pau -dnz 5 -df 4

# DESwl PAU + full sample
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_pau_full_sample_5.out    /usr/bin/python3 NzMarg_data.py --deswl_pau_full_sample -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_pau_full_sample_metacal_R_5.out    /usr/bin/python3 NzMarg_data.py --deswl_pau_full_sample_metacal_R -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/deswl_pau_full_sample_metacal_R_mlim_5.out    /usr/bin/python3 NzMarg_data.py --deswl_pau_full_sample_metacal_R_mlim -dnz 5

# Final run
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/final_5_df4.out   /usr/bin/python3 NzMarg_data.py --final -dnz 5 -df 4
# Fixed fitted bias
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/final_5_df4_fixed.out   /usr/bin/python3 NzMarg_data.py --final -dnz 5 -df 4
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/final_5_df2_fixed.out   /usr/bin/python3 NzMarg_data.py --final -dnz 5 -df 3
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/final_5_df2_fixed.out   /usr/bin/python3 NzMarg_data.py --final -dnz 5 -df 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/final_5_df1_fixed.out   /usr/bin/python3 NzMarg_data.py --final -dnz 5 -df 1


# HSC 
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_0.5.out /usr/bin/python3 NzMarg_data.py --hsc -dnz 0.5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_0.7.out /usr/bin/python3 NzMarg_data.py --hsc -dnz 0.7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_1.out   /usr/bin/python3 NzMarg_data.py --hsc -dnz 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_2.out   /usr/bin/python3 NzMarg_data.py --hsc -dnz 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_5.out   /usr/bin/python3 NzMarg_data.py --hsc -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_7.out   /usr/bin/python3 NzMarg_data.py --hsc -dnz 7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_10.out  /usr/bin/python3 NzMarg_data.py --hsc -dnz 10
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/hsc_15.out  /usr/bin/python3 NzMarg_data.py --hsc -dnz 15

# SD
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_1.out    /usr/bin/python3 NzMarg_data.py --SD -dnz 1
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_2.out    /usr/bin/python3 NzMarg_data.py --SD -dnz 2
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_5.out    /usr/bin/python3 NzMarg_data.py --SD -dnz 5
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_7.out    /usr/bin/python3 NzMarg_data.py --SD -dnz 7
# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_10.out   /usr/bin/python3 NzMarg_data.py --SD -dnz 10

# addqueue -s -c Tmat -n 1x12 -q $queue -m 1 -o log/SD_5_df2.out    /usr/bin/python3 NzMarg_data.py --SD -dnz 5 -df 2
