import sacc


def main(fname):
    if fname[-5:] == '.fits':
        fout = fname[:-5] + '_asindependent.fits'
    elif fname[-5:] == '.sacc':
        fout = fname[:-5] + '_asindependent.sacc'
    else:
        fout = fname + '_asindependent'

    s = sacc.Sacc.load_fits(fname)
    des = s.copy()
    des.remove_tracers([f'HSC__{i}' for i in range(4)])
    hsc = s.copy()
    hsc.remove_tracers([f'DESwl__{i}' for i in range(4)])

    sout = sacc.concatenate_data_sets(des, hsc)
    print(f'Saving asindependent file to: {fout}')
    sout.save_fits(fout)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Compute the N(z) marginalization")
    parser.add_argument('fname', type=str, help='DESxHSC sacc file')
    args = parser.parse_args()

    main(args.fname)
