#!/usr/bin/python3
import numpy as np
from scipy.interpolate import interp1d
import pyccl as ccl
import sacc
from nz_marginalization import NzMarg


def mag_bias(mlim):
    # HSC bias model
    # Eq. 4.11 of https://arxiv.org/pdf/1912.08209.pdf
    zs = np.linspace(0, 4, 1024)
    cosmo = ccl.CosmologyVanillaLCDM()
    bz = (0.8346+(mlim-24)*(-0.0624))/(ccl.growth_factor(cosmo, 1./(1+zs)))**1.3
    bzi_mag = interp1d(zs, bz)

    return bzi_mag


# DES phenomenological bias
def DES_bzi(cat, fixed_fit=True):
    if cat == 'metacal':
        if not fixed_fit:
            # v0 - wrong fit
            bi = [0.7, 0.88, 1.10, 1.11]
        else:
            # v1 - fixed fit
            bi = [0.84, 1.07, 1.39, 1.42]
    elif cat == 'redmagic':
        bi = [1.48, 1.76, 1.78, 2.19, 2.23]

    def bzi(z):
        z = np.asarray(z)
        z_edges = [(0.2, 0.43), (0.43, 0.63),
                   (0.63, 0.9), (0.9, 1.3)]

        bz = np.ones_like(z)
        # Let's use the same bias for z<zbin as in th first bin
        bz[z<0.2] *= bi[0]
        for i, zei  in enumerate(z_edges):
            z_i, z_e = zei
            bz[(z >= z_i) * (z < z_e)] *= bi[i]

        return bz

    return bzi


def DESgc_bzi(zbin):
    bi = [1.48, 1.76, 1.78, 2.19, 2.23]
    def bzi(z):
        return np.ones_like(z) * bi[zbin]

    return bzi


def eBOSS_bzi(zbin):
    bi = [2.1, 2.5]
    def bzi(z):
        return np.ones_like(z) * bi[zbin]

    return bzi


# These are for covar_master
biases = {'DESgc': DES_bzi('redmagic'),
          'DESwl': DES_bzi('metacal'),
          # mlim = 24.5 from HSC paper (https://arxiv.org/pdf/1809.09148.pdf), 22.5 from prelim_script/run.sh
          'HSC': mag_bias(24.5),
          'KiDS1000': mag_bias(25.02),  # 2007.01845
          }

# This are for the Cells computed with CCL
biases.update({f'DESgc__{i}': DESgc_bzi(i) for i in range(5)})
biases.update({f'eBOSS__{i}': eBOSS_bzi(i) for i in range(2)})


def DESwl_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/cls_DES_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl', outprefix='DESwl')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_NzMarg_full_sample(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_maxd_1_nngh_20_full_sample_nbins100_cls_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_full_sample', outprefix='DESwl_full_sample')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_NzMarg_full_sample_metacal_R(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_R_maxd_1_nngh_20_griz_full_sample_nbins100_cls_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_NzMarg_full_sample_metacal_R', outprefix='DESwl_NzMarg_full_sample_metacal_R')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_NzMarg_full_sample_metacal_R_mlim(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_R_maxd_1_nngh_20_griz_full_sample_nbins100_cls_covG_DIR.fits')
    biases['DESwl'] = mag_bias(22.5)

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_NzMarg_full_sample_metacal_R_mlim', outprefix='DESwl_NzMarg_full_sample_metacal_R_mlim')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_PAU_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_PAU_maxd_1_nngh_20_nbins100_cls_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_PAU', outprefix='DESwl_PAU')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_PAU_NzMarg_full_sample_metacal_R_mlim(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_PAU_R_maxd_1_nngh_20_griz_full_sample_nbins100_cls_covG_DIR.fits')
    biases['DESwl'] = mag_bias(22.5)

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_PAU_full_sample_metacal_R_mlim', outprefix='DESwl_PAU_full_sample_metacal_R_mlim')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_PAU_NzMarg_full_sample_metacal_R(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_PAU_R_maxd_1_nngh_20_griz_full_sample_nbins100_cls_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_PAU_full_sample_metacal_R', outprefix='DESwl_PAU_full_sample_metacal_R')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_PAU_NzMarg_full_sample(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/DES_PAU_maxd_1_nngh_20_griz_full_sample_nbins100_cls_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_PAU_full_sample', outprefix='DESwl_PAU_full_sample')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DESwl_ShahabNzs_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/DES/cls_DES_covG_ShahabNzs.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_ShahabNzs',
                outprefix='DESwl_ShahabNzs')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def DES_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('./data/cls_DES_gcwl_wlwl_covG_wlDIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2, diag_factor=diag_factor,
                outdir='./output/DESgcwl_wlwl', outprefix='DES_gcwl_wlwl',
                dir_tracers=[f'DESwl__{i}' for i in range(4)])
    Nz.compute_nz_covar()
    Nz.write_sacc()


def SD_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('./data/cls_SD_covG_wlDIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2, diag_factor=diag_factor,
                outdir='./output/SD', outprefix='SD',
                dir_tracers=[f'DESwl__{i}' for i in range(4)])
    Nz.compute_nz_covar()
    Nz.write_sacc()


def KiDS_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('../DIR/output/cls_K1000_covG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/KiDS', outprefix='K1000')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def HSC_NzMarg(dnz, diag_factor):
    HSC_bzi = mag_bias(22.5)

    s = sacc.Sacc.load_fits('./data/cls_signal_covG_HSC_Andrina_paper_newNz.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/HSC', outprefix='HSC')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def HSC_DESwl_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('./data/cls_DESwl_covG_HSC_covGNG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_HSC', outprefix='DESwl_HSC')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def HSC_DESwl_full_sample_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('./data/cls_DESwl_full_sample_covGNG_HSC_covGNG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_full_sample_HSC', outprefix='DESwl_full_sample_HSC')
    Nz.compute_nz_covar()
    Nz.write_sacc()


def HSC_DESwl_final_NzMarg(dnz, diag_factor):
    s = sacc.Sacc.load_fits('./data/cls_DESwl_final_covGNG_HSC_covGNG_DIR.fits')

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_final_fixed_HSC', outprefix='DESwl_final_fixed_HSC')
    Nz.compute_nz_covar()
    Nz.write_sacc()


    # Compute also the covariance assuming the mlim bias
    s = sacc.Sacc.load_fits('./data/cls_DESwl_final_covGNG_HSC_covGNG_DIR.fits')
    biases['DESwl'] = mag_bias(22.5)

    Nz = NzMarg(s, dnz, biases, area_deg2=2,
                diag_factor=diag_factor, outdir='./output/DESwl_final_mlim_HSC', outprefix='DESwl_final_mlim_HSC')
    Nz.compute_nz_covar()
    Nz.write_sacc()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Compute the N(z) marginalization")
    parser.add_argument('-df', '--diag_factor', type=float, default=1, help='Diagonal factor to multiply the diagonal')
    parser.add_argument('-dnz', '--dnz', type=float, default=5., help='z shift for z differentiation')
    parser.add_argument('--SD', default=False, action='store_true', help='SD')
    parser.add_argument('--des', default=False, action='store_true', help='DES (gcwl + wlwl)')
    parser.add_argument('--deswl', default=False, action='store_true', help='DESwl')
    parser.add_argument('--deswl_pau', default=False, action='store_true', help='DESwl Nz calibrated with PAU')
    parser.add_argument('--deswl_full_sample', default=False, action='store_true', help='DESwl Nz calibrated with COSMOS15 and the full sample')
    parser.add_argument('--deswl_full_sample_metacal_R', default=False, action='store_true', help='DESwl Nz calibrated with COSMOS15 and the full sample and adding the metacal R weights')
    parser.add_argument('--deswl_full_sample_metacal_R_mlim', default=False, action='store_true', help='DESwl Nz calibrated with COSMOS15 and the full sample and adding the metacal R weights and mlim for the bias')
    parser.add_argument('--deswl_pau_full_sample', default=False, action='store_true', help='DESwl Nz calibrated with PAU and the full sample')
    parser.add_argument('--deswl_pau_full_sample_metacal_R', default=False, action='store_true', help='DESwl Nz calibrated with PAU and the full sample and adding the metacal R weights')
    parser.add_argument('--deswl_pau_full_sample_metacal_R_mlim', default=False, action='store_true', help='DESwl Nz calibrated with PAU and the full sample and adding the metacal R weights and mlim for the bias')
    parser.add_argument('--kids', default=False, action='store_true', help='KiDS')
    parser.add_argument('--hsc', default=False, action='store_true', help='HSC')
    parser.add_argument('--shahab', default=False, action='store_true', help='DESwl with Shahab Nz')
    parser.add_argument('--final', default=False, action='store_true', help='DESwl_final with HSC')
    args = parser.parse_args()

    fargs = (args.dnz, args.diag_factor)
    if args.final:
        HSC_DESwl_final_NzMarg(*fargs)
    elif args.deswl and args.hsc:
        HSC_DESwl_NzMarg(*fargs)
    elif args.deswl_full_sample and args.hsc:
        HSC_DESwl_full_sample_NzMarg(*fargs)
    elif args.SD:
        SD_NzMarg(*fargs)
    elif args.des:
        DES_NzMarg(*fargs)
    elif args.deswl:
        DESwl_NzMarg(*fargs)
    elif args.hsc:
        HSC_NzMarg(*fargs)
    elif args.kids:
        KiDS_NzMarg(*fargs)
    elif args.shahab:
        DESwl_ShahabNzs_NzMarg(*fargs)
    elif args.deswl_pau:
        DESwl_PAU_NzMarg(*fargs)
    elif args.deswl_full_sample:
        DESwl_NzMarg_full_sample(*fargs)
    elif args.deswl_full_sample_metacal_R:
        DESwl_NzMarg_full_sample_metacal_R(*fargs)
    elif args.deswl_full_sample_metacal_R_mlim:
        DESwl_NzMarg_full_sample_metacal_R_mlim(*fargs)
    elif args.deswl_pau_full_sample:
        DESwl_PAU_NzMarg_full_sample(*fargs)
    elif args.deswl_pau_full_sample_metacal_R:
        DESwl_PAU_NzMarg_full_sample_metacal_R(*fargs)
    elif args.deswl_pau_full_sample_metacal_R_mlim:
        DESwl_PAU_NzMarg_full_sample_metacal_R_mlim(*fargs)
