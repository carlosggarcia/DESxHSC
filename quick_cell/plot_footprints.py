#!/usr/bin/python

import numpy as np
import pymaster as nmt
import fitsio
import healpy as hp
import sys
sys.path.append('../../xCell/')
import xcell
import pyccl as ccl
from matplotlib import pyplot as plt

# Load COSMOS15
path_spec_sample = '/mnt/extraspace/damonge/S8z_data/COSMOS/COSMOS2015_Laigle+_v1.1.fits'
cat30 = fitsio.read(path_spec_sample, columns=['ALPHA_J2000', 'DELTA_J2000'])
map_cat30 = xcell.mappers.utils.get_map_from_points(cat30, 512, ra_name='ALPHA_J2000', dec_name='DELTA_J2000')

# Load KiDS (WL catalog does not overlap. Use this tile)
# kids = fitsio.read('./KiDS_DR4.0_150.1_2.2_ugriZYJHKs_cat.fits', columns=['ALPHA_J2000', 'DELTA_J2000'])
# mask = xcell.mappers.utils.get_map_from_points(kids, 512, ra_name='ALPHA_J2000', dec_name= 'DELTA_J2000')

# Load DES
# path_photo_sample_sky_short = '/mnt/extraspace/gravityls_3/data/DESY1/mcal-y1a1-combined-riz-unblind-v4-matched_short_COSMOS.fits'
# des = fitsio.read(path_photo_sample_sky_short)
# mask = xcell.mappers.utils.get_map_from_points(des, 512, ra_name='ra', dec_name='dec')

# Load RedMaGiC
path = '/mnt/extraspace/damonge/S8z_data/DES_data/redmagic_catalog/DES_Y1A1_3x2pt_redMaGiC_zerr_CATALOG.fits'
red = fitsio.read(path, columns=['RA', 'DEC'])
mask = xcell.mappers.utils.get_map_from_points(red, 512, ra_name='RA', dec_name='DEC')

# Give to the footprints a identifiable value
mask[mask>0] = 1
map_cat30[map_cat30 > 0] = 2

print(f'(mask + map_cat30).max = {(mask + map_cat30).max()}', flush=True)

hp.cartview(mask + map_cat30, lonra=[cat30['ALPHA_J2000'].min() - 10/60, cat30['ALPHA_J2000'].max() + 10/60], latra=[cat30['DELTA_J2000'].min() - 10/60, cat30['DELTA_J2000'].max() + 10/60])


#hp.cartview(mask + map_cat30, lonra=[des['ra'].min(), des['ra'].max()], latra=[des['dec'].min(), des['dec'].max()])
# plt.savefig('DESwl_COSMOS30.png')

#plt.savefig('K1000_COSMOS30.png')
plt.savefig('DESredmagic_COSMOS30.png')
plt.show()
plt.close()


