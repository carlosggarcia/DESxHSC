#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')

import numpy as np
import pymaster as nmt
import fitsio
import healpy as hp
import sys
sys.path.append('../../xCell/')
import xcell
import pyccl as ccl
from matplotlib import pyplot as plt

columns = [f'MAG_GAAP_{i}' for i in 'ugriZYJH'] + ['MAG_GAAP_Ks']

k1000_cos30_tile = fitsio.read('./KiDS_DR4.0_150.1_2.2_ugriZYJHKs_cat.fits', columns=columns)

k1000_tile2 = fitsio.read('KiDS_DR4.0_200.0_0.5_ugriZYJHKs_cat.fits', columns=columns)

k1000_cat = fitsio.read('/mnt/extraspace/damonge/S8z_data/KiDS_1000/KiDS_DR4.1_ugriZYJHKs_SOM_gold_WL_cat.fits', columns=columns + ['weight'])
for c in columns:
    k1000_cat = k1000_cat[k1000_cat[c] < 50]

# Remove r > k1000_cat['MAG_GAAP_r'].max()
# rmax = k1000_cat['MAG_GAAP_r'].max()
rmax = 25 # k1000_cat['MAG_GAAP_r'].max()
k1000_cos30_tile = k1000_cos30_tile[k1000_cos30_tile['MAG_GAAP_r'] < rmax]
k1000_tile2 = k1000_tile2[k1000_tile2['MAG_GAAP_r'] < rmax]

for c in columns:
    weights = [np.ones(len(k1000_cos30_tile)), np.ones(len(k1000_tile2)), k1000_cat['weight']]
    print(f'COSMOS30 band {c} is nan?: {np.all(np.isnan(k1000_cos30_tile[c]))}')
    plt.hist([k1000_cos30_tile[c], k1000_tile2[c], k1000_cat[c]],
             weights=weights, bins=100, density=True, histtype='step',
             label=['COSMOS30 tile', 'tile 2', 'WL cat']) #, linestyle=['solid', 'dashed', 'solid'])
    plt.title(c + ' (rmax)')
    plt.xlabel('mag')
    plt.legend(loc=0)
    plt.savefig(f'K1000_{c}_hist_rmax.png')
    # plt.show()
    plt.close()
