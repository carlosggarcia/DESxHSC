#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')

import numpy as np
import pymaster as nmt
import fitsio
import healpy as hp
import sys
sys.path.append('../../xCell/')
import xcell
import pyccl as ccl
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d

def load_metacal(zbin, return_noise=True, return_dndz=True, plot_footprint=False):
    # Load METACAL
    print('Loading METACAL', flush=True)
    # Load map of N gals per pixel (the mask in METACAL case)
    map_N = hp.read_map(f'/mnt/extraspace/gravityls_3/S8z/data/DES/DESwlMETACAL_mask_zbin{zbin}_ns512.fits.gz')
    mask = hp.read_map('/mnt/extraspace/damonge/Datasets/DES_Y1/redmagic_catalog/DES_Y1A1_3x2pt_redMaGiC_MASK_HPIX4096RING.fits')
    mask[mask < 0] = 0
    mask = hp.ud_grade(mask, nside_out=512)

    if plot_footprint:
        a = np.zeros_like(map_N)
        b = np.zeros_like(map_N)
        a[map_N > 0] += 1
        b[mask > 0] += 2
        hp.mollview(a + b)
        plt.savefig('metacal_redmagic_footprint.png')

    # weights=1 for METACAL
    goodpix = mask > 0
    N_mean = np.sum(map_N[goodpix]) / np.sum(mask[goodpix])

    map_delta = np.zeros_like(map_N)
    map_delta[goodpix] = map_N[goodpix] / (mask[goodpix] * N_mean) - 1

    field_delta = nmt.NmtField(mask, [map_delta], spin=0, n_iter=0)

    output = [map_delta, mask, field_delta]

    if return_noise:
        goodpix = mask > 0  # Already capped at mask_threshold
        npix = hp.nside2npix(512)
        N_mean_srad = N_mean / (4 * np.pi) * npix
        correction = 1  # map_delta[goodpix].sum()/map_delta[goodpix].sum()
        N_ell = correction * np.mean(mask) / N_mean_srad
        nl_dd_cp = N_ell * np.ones((1, 3*512))
        output.append(nl_dd_cp)

    if return_dndz:
        dndz_file =  '/mnt/extraspace/damonge/Datasets/DES_Y1/shear_catalog/y1_redshift_distributions_v1.fits'
        dndz = fitsio.read(dndz_file)
        # import sacc
        # s = sacc.Sacc.load_fits('../nz_marginalization/data/cls_DESwl_final_covGNG_HSC_covGNG_DIR.fits')
        # dndz = {}
        # dndz["Z_MID"] = s.tracers['DESwl__0'].z
        # for i in range(4):
        #     dndz[f"BIN{i+1}"] = s.tracers[f'DESwl__{i}'].nz
        output.append(dndz)

    return output


def load_kids(zbin, return_noise=True, return_dndz=True):
    # Load METACAL
    print('Loading K1000', flush=True)
    cat = fitsio.read(f'/mnt/extraspace/gravityls_3/S8z/data/derived_products/KiDS1000_lite_cat_zbin{zbin}.fits')
    map_N = xcell.mappers.utils.get_map_from_points(cat, 512, ra_name='ALPHA_J2000', dec_name='DELTA_J2000')
    mask = np.zeros_like(map_N)
    mask[map_N > 0] = 1
    mask = nmt.mask_apodization(mask, 0.2)
    mask = hp.ud_grade(mask, nside_out=512)

    # weights=1 for METACAL
    goodpix = mask > 0
    N_mean = np.sum(map_N[goodpix]) / np.sum(mask[goodpix])

    map_delta = np.zeros_like(map_N)
    map_delta[goodpix] = map_N[goodpix] / (mask[goodpix] * N_mean) - 1

    field_delta = nmt.NmtField(mask, [map_delta], spin=0, n_iter=0)

    output = [map_delta, mask, field_delta]

    if return_noise:
        goodpix = mask > 0  # Already capped at mask_threshold
        npix = hp.nside2npix(512)
        N_mean_srad = N_mean / (4 * np.pi) * npix
        correction = 1  # map_delta[goodpix].sum()/map_delta[goodpix].sum()
        N_ell = correction * np.mean(mask) / N_mean_srad
        nl_dd_cp = N_ell * np.ones((1, 3*512))
        output.append(nl_dd_cp)

    if return_dndz:
        dndz_file =  f'/mnt/extraspace/damonge/S8z_data/KiDS_1000/SOM_N_of_Z/K1000_NS_V1.0.0A_ugriZYJHKs_photoz_SG_mask_LF_svn_309c_2Dbins_v2_SOMcols_Fid_blindC_TOMO{zbin+1}_Nz.asc'
        dndz = np.loadtxt(dndz_file, unpack=True)[:2]
        output.append({'Z_MID': dndz[0], f'BIN{zbin+1}': dndz[1]})

    return output


def load_cmbk():
    # Load CMBkappa
    print('Loading CMBkappa', flush=True)
    config = {
        'file_klm': '/mnt/extraspace/damonge/S8z_data/Planck_data/COM_Lensing_4096_R3.00/MV/dat_klm.fits',
        'file_mask': '/mnt/extraspace/damonge/S8z_data/Planck_data/COM_Lensing_4096_R3.00/mask.fits.gz',
        'file_noise': '/mnt/extraspace/damonge/S8z_data/Planck_data/COM_Lensing_4096_R3.00/MV/nlkk.dat',
        'nside': 512,
        'mask_name': 'mask_PLAcv',
        'mapper_class': 'MapperP18CMBK',
        'mask_aposize': 0.5,
        # 'path_rerun': '/mnt/extraspace/gravityls_3/S8z/data/derived_products/planck_lensing/COM_Lensing_4096_R3.00/',
        'coords': 'C'
    }

    m = xcell.mappers.MapperP18CMBK(config)
    map_kappa = m.get_signal_map()
    mask_kappa = m.get_mask()

    # field_kappa = nmt.NmtField(mask_kappa, map_kappa, spin=0, n_iter=0)
    field_kappa = m.get_nmt_field()

    return map_kappa, mask_kappa, field_kappa

def load_workspaces(bins, field_delta, field_kappa):
    print('Loading Workspace', flush=True)
    w_dd = nmt.NmtWorkspace()
    w_dd.compute_coupling_matrix(field_delta, field_delta, bins)

    w_dk = nmt.NmtWorkspace()
    w_dk.compute_coupling_matrix(field_delta, field_kappa, bins)

    w_kk = nmt.NmtWorkspace()
    w_kk.compute_coupling_matrix(field_kappa, field_kappa, bins)

    return w_dd, w_dk, w_kk

def get_theory_Cells(zbin, w_dd, w_dk, w_kk, nl_dd_cp, dndz):
    # Compute theoretical Cell
    print('Computing Cells and covariance', flush=True)

    cosmo = ccl.CosmologyVanillaLCDM()

    bias = (dndz['Z_MID'], np.ones_like(dndz['Z_MID']))
    tracer_delta = ccl.tracers.NumberCountsTracer(cosmo, has_rsd=False, dndz=(dndz['Z_MID'], dndz[f'BIN{zbin+1}']), bias=bias, mag_bias=None)
    tracer_kappa = ccl.tracers.CMBLensingTracer(cosmo, 1100)

    ell = np.arange(512 * 3)
    cl_th_dk = np.array([ccl.angular_cl(cosmo, tracer_delta, tracer_kappa, ell)])

    cl_th_dk = w_dk.decouple_cell(w_dk.couple_cell(cl_th_dk))

    cl_th_dd = np.array([ccl.angular_cl(cosmo, tracer_delta, tracer_delta, ell)])
    cl_th_dd = w_dd.decouple_cell(w_dd.couple_cell(cl_th_dd) + nl_dd_cp)

    cl_th_kk = np.array([ccl.angular_cl(cosmo, tracer_kappa, tracer_kappa, ell)])
    cl_th_kk = w_kk.decouple_cell(w_kk.couple_cell(cl_th_kk)) # + m.get_nl_coupled()/np.mean(mask)**2)
    cl_kk_file = np.loadtxt('/mnt/extraspace/damonge/S8z_data/Planck_data/COM_Lensing_4096_R3.00/MV/nlkk.dat',
               unpack=True)

    l_bpw = get_lbpw()
    cl_th_kk += interp1d(cl_kk_file[0], cl_kk_file[1], bounds_error=False,
                         fill_value='extrapolate')(l_bpw)

    # print('cl_th_dd', cl_th_dd, flush=True)
    # print('cl_th_kk', cl_th_kk, flush=True)
    # print('cl_th_dk', cl_th_dk, flush=True)

    return cl_th_dk, cl_th_dd, cl_th_kk


def get_lbpw():
    return np.load('/mnt/extraspace/gravityls_3/S8z/Cls_new_pipeline/512_DES_eBOSS_CMB/DESgc_PLAcv/cl_DESgc__0_PLAcv.npz')['ell']


def main(zbin, survey='metacal'):
    bpw_edges = np.array([0, 30, 60, 90, 120, 150, 180, 210, 240, 272, 309, 351, 398, 452, 513, 582, 661, 750, 852, 967, 1098, 1247, 1416, 1536])
    bins = nmt.NmtBin.from_edges(bpw_edges[:-1], bpw_edges[1:])
    delta_ell = np.diff(bpw_edges)
    l_bpw = get_lbpw()
    if survey == 'metacal':
        map_delta, mask_delta, field_delta, nl_dd_cp, dndz = load_metacal(zbin, return_noise=True, return_dndz=True)
    elif survey == 'k1000':
        map_delta, mask_delta, field_delta, nl_dd_cp, dndz = load_kids(zbin, return_noise=True, return_dndz=True)
    map_kappa, mask_kappa, field_kappa = load_cmbk()
    w_dd, w_dk, w_kk = load_workspaces(bins, field_delta, field_kappa)
    cl_th_dk, cl_th_dd, cl_th_kk = get_theory_Cells(zbin, w_dd, w_dk, w_kk, nl_dd_cp, dndz)

    # Compute coupled Cell
    cl_dk_cp = nmt.compute_coupled_cell(field_delta, field_kappa)
    # Decouple cell
    cl_dk = w_dk.decouple_cell(cl_dk_cp)

    # Compute the error with the Knox formula
    fsky = np.mean((mask_delta > 0) & (mask_kappa > 0))
    print(f'fsky = {fsky}', flush=True)
    cov = (cl_th_dd[0] * cl_th_kk[0] + cl_th_dk[0] * cl_th_dk[0]) / (fsky * (2 * l_bpw + 1) * delta_ell)
    sigma_dk = np.sqrt(cov)  # The Knox formula only fills the diagonal
    print(sigma_dk/cl_dk)

    # Fit bias
    cl_th_dk_int = interp1d(l_bpw, cl_th_dk[0])

    def f(x, b):
        return b * cl_th_dk_int(x)

    print('Fitting', flush=True)
    popt, pcov = curve_fit(f, l_bpw, cl_dk[0], sigma=sigma_dk, absolute_sigma=True)

    print(f'b = {popt[0]}', flush=True)


    # Plot
    print('Plotting', flush=True)
    f, ax = plt.subplots(2, 1, gridspec_kw={'hspace':0}, sharex=True)
    ax[0].errorbar(l_bpw, cl_dk[0], yerr=sigma_dk, fmt='.', color='k')
    ax[0].loglog(l_bpw, cl_th_dk[0], label=r'$b = 1$')
    ax[0].loglog(l_bpw, popt[0] * cl_th_dk[0], '--', label=f'$b = {popt[0]:.2f}$')
    ax[1].semilogx(l_bpw, (cl_dk[0] - cl_th_dk[0]) / sigma_dk)
    ax[1].semilogx(l_bpw, (cl_dk[0] - popt[0] * cl_th_dk[0]) / sigma_dk, '--')
    ax[1].axhline(0, ls='--', color='gray')
    ax[1].set_xlabel(r'$\ell$')
    ax[0].set_ylabel(r'$C_\ell$')
    ax[1].set_ylabel(r'(Data - Theory) / $\sigma$')
    ax[0].set_title(r'$\delta_{}$-$\kappa$'.format(zbin))
    ax[0].legend()
    plt.savefig(f'{survey}_gc_{zbin}_fixed.png')
    # plt.savefig(f'{survey}_gc_{zbin}_fixed_DIR.png')
    # plt.show()
    plt.close()

    print('The End', flush=True)


if __name__ == '__main__':
    survey = sys.argv[1]
    zbin = int(sys.argv[2])
    main(zbin, survey)
