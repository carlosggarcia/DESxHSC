#!/usr/bin/python3

import sacc
import numpy as np

des = sacc.Sacc.load_fits('../DIR/output/cls_DES_covG_DIR.fits')
hsc = sacc.Sacc.load_fits('/mnt/extraspace/damonge/S8z_data/HSC/HSC_DR1/output_4096/cls_cov.fits')

# Check HSC nz's
for trn, tr in hsc.tracers.items():
    print(trn, np.sum(tr.nz * (tr.z[1] - tr.z[0])))

hsc.remove_selection(data_type='cl_eb')
hsc.remove_selection(data_type='cl_be')
hsc.remove_selection(data_type='cl_bb')

des_hsc = sacc.concatenate_data_sets(des, hsc)
des_hsc.save_fits('output/cls_DES_HSC_covG_DIR.fits')
